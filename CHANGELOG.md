# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

[1.3.0] - 2020-06-23
### Added
- Concept Association added to excel import
- relationtype enum added

## [1.2.0] - 2020-04-29
### Changed
- restrict languages to predefined values
- apply google java code style
### Fixed
- empty strings in xsd import bug fixed

## [1.1.1] - 2019-09-06
### Changed
- String, Integer and Float validation without validation id will no longer be TBD, but the selected 
validation without constraints (no min/max, no regex)

## [1.1.0] - 2019-09-05
### Added
- ValidationType "TBD" can be chosen and will be used for any validation with missing id as well
### Changed
- Accept "id" as column name for dataelements
- Slightly changed changelog format and name

## [1.0.6] - 2019-04-05
### Added
- initial release