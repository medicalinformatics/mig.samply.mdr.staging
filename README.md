# MDR Staging
> Read Dataelements from Excel sheets.

Offers methods to read from and write to excel files, as well as conversions between excel 
and xml format. The excel files need to be in the correct format. See Usage example below.

## Installation

Maven:

```xml
<dependency>
    <groupId>de.mig.samply</groupId>
    <artifactId>mdr-staging</artifactId>
    <version>$VERSION</version>
</dependency>
```


## Usage example

Excel files need to be in the correct format. To generate an example file (filled with random values),
use `FileWriterExcelTest.testCreateWorkbook()`.

To read an excel file, simply create a new instance of `FileReaderExcel` and pass the file in the
constructor.

```java
FileReaderExcel fileReaderExcel = new FileReaderExcel(importFile);
```

Alternatively, you can pass the filename instead of the file.

```java
FileReaderExcel fileReaderExcel = new FileReaderExcel("/path/to/file");
```

After reading the file, the `FileReaderExcel` instance now contains an ArrayList of all dataelements,
dataelement groups and records found in the excel file.

Use `ImportToStagingConverter.excelDataelementToStaging()` to convert the dataelements to the correct
format to use in the MDR.

## Release History

see [CHANGELOG](CHANGELOG.md)

## Meta

Distributed under the GNU Affero General Public License v3. See [LICENSE](LICENSE.md) for more information.

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull