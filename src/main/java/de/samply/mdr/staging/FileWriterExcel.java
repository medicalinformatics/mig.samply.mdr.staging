/*
 * Copyright (C) 2018 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.staging;

import de.samply.mdr.staging.Constants.Sheets;
import de.samply.mdr.staging.model.enums.ValidationType;
import de.samply.mdr.staging.model.source.excel.ConceptAssociation;
import de.samply.mdr.staging.model.source.excel.Dataelement;
import de.samply.mdr.staging.model.source.excel.Definition;
import de.samply.mdr.staging.model.source.excel.Slot;
import de.samply.mdr.staging.model.validators.CalendarValidation;
import de.samply.mdr.staging.model.validators.CatalogValidation;
import de.samply.mdr.staging.model.validators.FloatValidation;
import de.samply.mdr.staging.model.validators.IntegerValidation;
import de.samply.mdr.staging.model.validators.PermittedValue;
import de.samply.mdr.staging.model.validators.PermittedValuesValidation;
import de.samply.mdr.staging.model.validators.StringValidation;
import de.samply.mdr.staging.utils.ExcelUtil;
import de.samply.mdr.xsd.staging.ElementType;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FileWriterExcel {

  private static final Logger logger = LoggerFactory.getLogger(FileWriterExcel.class);

  private static int definitionsId;
  private Workbook workbook;
  private Map<String, Map<String, Integer>> assignments;

  public FileWriterExcel() {
    reInit();
  }

  private void reInit() {
    definitionsId = 1;
    workbook = new XSSFWorkbook();
    assignments = new HashMap<>();
  }

  /**
   * TODO: add javadoc.
   */
  public void createWorkbook(List<Dataelement> dataelements) {
    reInit();
    initializeWorkbook();
    createInfoSheet();
    addDataelementsToWorkbook(dataelements);
    ExcelUtil.autosizeAllColumns(workbook);
  }

  private void addDataelementsToWorkbook(List<Dataelement> dataelements) {
    for (Dataelement dataelement : dataelements) {
      addEntryToWorkbook(dataelement);
    }
  }

  private void addEntryToWorkbook(Dataelement dataelement) {
    final int thisDefinitionId = definitionsId++;
    Map<String, Integer> assignment;

    // The dataelement sheet entry
    Sheet dataElementsSheet = workbook.getSheet(Constants.NAME_SHEET_DATAELEMENTS);
    assignment = assignments.get(Constants.NAME_SHEET_DATAELEMENTS);

    Row dataelementRow = dataElementsSheet.createRow(dataElementsSheet.getLastRowNum() + 1);
    dataelementRow
        .createCell(assignment.get(Constants.Sheets.Dataelements.COLNAME_ID), CellType.STRING)
        .setCellValue(Integer.toString(dataelement.getId()));
    dataelementRow
        .createCell(
            assignment.get(Constants.Sheets.Dataelements.COLNAME_ELEMENT_TYPE), CellType.STRING)
        .setCellValue(dataelement.getElementType().value());
    if (dataelement.getParentId() != null) {
      dataelementRow
          .createCell(
              assignment.get(Constants.Sheets.Dataelements.COLNAME_PARENT_ID), CellType.STRING)
          .setCellValue(Integer.toString(dataelement.getParentId()));
    }
    dataelementRow
        .createCell(
            assignment.get(Constants.Sheets.Dataelements.COLNAME_DEFINITION_ID), CellType.STRING)
        .setCellValue(Integer.toString(thisDefinitionId));

    // Only Elements have validations
    if (dataelement.getElementType() == ElementType.DATAELEMENT) {
      dataelementRow
          .createCell(
              assignment.get(Constants.Sheets.Dataelements.COLNAME_VALIDATION_TYPE),
              CellType.STRING)
          .setCellValue(dataelement.getValidation().getValidationType().getValidationName());

      // Boolean validation has no sheet since it has no options
      if (dataelement.getValidation().getValidationType() != ValidationType.VT_BOOLEAN) {
        dataelementRow
            .createCell(
                assignment.get(Constants.Sheets.Dataelements.COLNAME_VALIDATION_ID),
                CellType.STRING)
            .setCellValue(Integer.toString(dataelement.getValidation().getId()));
      }
    }

    // The definitions entries
    for (Definition definition : dataelement.getDefinitions()) {
      Sheet definitionSheet = workbook.getSheet(Constants.NAME_SHEET_DEFINITIONS);
      assignment = assignments.get(Constants.NAME_SHEET_DEFINITIONS);

      Row definitionRow = definitionSheet.createRow(definitionSheet.getLastRowNum() + 1);
      definitionRow
          .createCell(assignment.get(Constants.Sheets.Definitions.COLNAME_ID), CellType.STRING)
          .setCellValue(Integer.toString(thisDefinitionId));
      definitionRow
          .createCell(
              assignment.get(Constants.Sheets.Definitions.COLNAME_LANGUAGE), CellType.STRING)
          .setCellValue(definition.getLanguage().getName());
      definitionRow
          .createCell(
              assignment.get(Constants.Sheets.Definitions.COLNAME_DESIGNATION), CellType.STRING)
          .setCellValue(definition.getDesignation());
      definitionRow
          .createCell(
              assignment.get(Constants.Sheets.Definitions.COLNAME_DEFINITION), CellType.STRING)
          .setCellValue(definition.getDefinition());
    }

    if (dataelement.getValidation() != null) {
      // The validations entry
      switch (dataelement.getValidation().getValidationType()) {
        case VT_STRING:
          Sheet stringValidationSheet = workbook.getSheet(Constants.NAME_SHEET_VALIDATIONS_STRING);
          assignment = assignments.get(Constants.NAME_SHEET_VALIDATIONS_STRING);

          Row stringValidationRow =
              stringValidationSheet.createRow(stringValidationSheet.getLastRowNum() + 1);
          StringValidation stringValidation = (StringValidation) dataelement.getValidation();

          stringValidationRow
              .createCell(assignment.get(Constants.Sheets.Validations.COLNAME_ID), CellType.STRING)
              .setCellValue(stringValidation.getId());
          stringValidationRow
              .createCell(
                  assignment.get(Constants.Sheets.Validations.ValidationsString.COLNAME_MAXLENGTH),
                  CellType.STRING)
              .setCellValue(Integer.toString(stringValidation.getMaxLength()));
          stringValidationRow
              .createCell(
                  assignment.get(Constants.Sheets.Validations.ValidationsString.COLNAME_REGEX),
                  CellType.STRING)
              .setCellValue(stringValidation.getRegex());
          break;
        case VT_INTEGER:
          Sheet integerValidationSheet =
              workbook.getSheet(Constants.NAME_SHEET_VALIDATIONS_INTEGER);
          assignment = assignments.get(Constants.NAME_SHEET_VALIDATIONS_INTEGER);

          Row integerValidationRow =
              integerValidationSheet.createRow(integerValidationSheet.getLastRowNum() + 1);
          IntegerValidation integerValidation = (IntegerValidation) dataelement.getValidation();

          integerValidationRow
              .createCell(assignment.get(Constants.Sheets.Validations.COLNAME_ID), CellType.STRING)
              .setCellValue(integerValidation.getId());
          integerValidationRow
              .createCell(
                  assignment.get(
                      Constants.Sheets.Validations.ValidationsNumeric.COLNAME_RANGE_FROM),
                  CellType.STRING)
              .setCellValue(Long.toString(integerValidation.getRangeFrom()));
          integerValidationRow
              .createCell(
                  assignment.get(Constants.Sheets.Validations.ValidationsNumeric.COLNAME_RANGE_TO),
                  CellType.STRING)
              .setCellValue(Long.toString(integerValidation.getRangeTo()));
          integerValidationRow
              .createCell(
                  assignment.get(
                      Constants.Sheets.Validations.ValidationsNumeric.COLNAME_UNIT_OF_MEASURE),
                  CellType.STRING)
              .setCellValue(integerValidation.getUnitOfMeasure());
          break;
        case VT_FLOAT:
          Sheet floatValidationSheet = workbook.getSheet(Constants.NAME_SHEET_VALIDATIONS_FLOAT);
          assignment = assignments.get(Constants.NAME_SHEET_VALIDATIONS_FLOAT);

          Row floatValidationRow =
              floatValidationSheet.createRow(floatValidationSheet.getLastRowNum() + 1);
          FloatValidation floatValidation = (FloatValidation) dataelement.getValidation();

          floatValidationRow
              .createCell(assignment.get(Constants.Sheets.Validations.COLNAME_ID), CellType.STRING)
              .setCellValue(floatValidation.getId());
          floatValidationRow
              .createCell(
                  assignment.get(
                      Constants.Sheets.Validations.ValidationsNumeric.COLNAME_RANGE_FROM),
                  CellType.STRING)
              .setCellValue(Double.toString(floatValidation.getRangeFrom()));
          floatValidationRow
              .createCell(
                  assignment.get(Constants.Sheets.Validations.ValidationsNumeric.COLNAME_RANGE_TO),
                  CellType.STRING)
              .setCellValue(Double.toString(floatValidation.getRangeTo()));
          floatValidationRow
              .createCell(
                  assignment.get(
                      Constants.Sheets.Validations.ValidationsNumeric.COLNAME_UNIT_OF_MEASURE),
                  CellType.STRING)
              .setCellValue(floatValidation.getUnitOfMeasure());
          break;
        case VT_CALENDAR:
        case VT_DATE:
        case VT_DATETIME:
        case VT_TIME:
          Sheet calendarValidationSheet =
              workbook.getSheet(Constants.NAME_SHEET_VALIDATIONS_CALENDAR);
          assignment = assignments.get(Constants.NAME_SHEET_VALIDATIONS_CALENDAR);

          Row calendarValidationRow =
              calendarValidationSheet.createRow(calendarValidationSheet.getLastRowNum() + 1);
          CalendarValidation calendarValidation = (CalendarValidation) dataelement.getValidation();

          calendarValidationRow
              .createCell(assignment.get(Constants.Sheets.Validations.COLNAME_ID), CellType.STRING)
              .setCellValue(calendarValidation.getId());
          calendarValidationRow
              .createCell(
                  assignment.get(Constants.Sheets.Validations.ValidationsCalendar.COLNAME_FORMAT),
                  CellType.STRING)
              .setCellValue(calendarValidation.getDateTimeFormat().getFormatString());
          break;
        case VT_BOOLEAN:
          // Boolean validation has no sheet since it has no options
          break;
        case VT_CATALOG:
          Sheet catalogValidationSheet =
              workbook.getSheet(Constants.NAME_SHEET_VALIDATIONS_CATALOG);
          assignment = assignments.get(Constants.NAME_SHEET_VALIDATIONS_CATALOG);

          Row catalogValidationRow =
              catalogValidationSheet.createRow(catalogValidationSheet.getLastRowNum() + 1);
          CatalogValidation catalogValidation = (CatalogValidation) dataelement.getValidation();

          catalogValidationRow
              .createCell(assignment.get(Constants.Sheets.Validations.COLNAME_ID), CellType.STRING)
              .setCellValue(catalogValidation.getId());
          catalogValidationRow
              .createCell(
                  assignment.get(
                      Constants.Sheets.Validations.ValidationsCatalog.COLNAME_CATALOG_URN),
                  CellType.STRING)
              .setCellValue(catalogValidation.getCatalogUrn());
          break;
        case VT_ENUMERATED:
          Sheet permittedValuesValidationSheet =
              workbook.getSheet(Constants.NAME_SHEET_VALIDATIONS_PERMITTEDVALUES);

          PermittedValuesValidation permittedValuesValidation =
              (PermittedValuesValidation) dataelement.getValidation();

          for (PermittedValue pv : permittedValuesValidation.getPermittedValues()) {
            assignment = assignments.get(Constants.NAME_SHEET_VALIDATIONS_PERMITTEDVALUES);
            Row permittedValuesValidationRow =
                permittedValuesValidationSheet.createRow(
                    permittedValuesValidationSheet.getLastRowNum() + 1);

            permittedValuesValidationRow
                .createCell(
                    assignment.get(Constants.Sheets.Validations.COLNAME_ID), CellType.STRING)
                .setCellValue(permittedValuesValidation.getId());

            permittedValuesValidationRow
                .createCell(
                    assignment.get(Constants.Sheets.Validations.PermittedValues.COLNAME_VALUE),
                    CellType.STRING)
                .setCellValue(pv.getValue());

            int pvDefinitionsId = definitionsId++;

            permittedValuesValidationRow
                .createCell(
                    assignment.get(
                        Constants.Sheets.Validations.PermittedValues.COLNAME_DEFINITION_ID),
                    CellType.STRING)
                .setCellValue(pvDefinitionsId);

            for (Definition definition : pv.getDefinitions()) {
              Sheet definitionSheet = workbook.getSheet(Constants.NAME_SHEET_DEFINITIONS);
              assignment = assignments.get(Constants.NAME_SHEET_DEFINITIONS);

              Row definitionRow = definitionSheet.createRow(definitionSheet.getLastRowNum() + 1);
              definitionRow
                  .createCell(
                      assignment.get(Constants.Sheets.Definitions.COLNAME_ID), CellType.STRING)
                  .setCellValue(Integer.toString(pvDefinitionsId));
              definitionRow
                  .createCell(
                      assignment.get(Constants.Sheets.Definitions.COLNAME_LANGUAGE),
                      CellType.STRING)
                  .setCellValue(definition.getLanguage().getName());
              definitionRow
                  .createCell(
                      assignment.get(Constants.Sheets.Definitions.COLNAME_DESIGNATION),
                      CellType.STRING)
                  .setCellValue(definition.getDesignation());
              definitionRow
                  .createCell(
                      assignment.get(Constants.Sheets.Definitions.COLNAME_DEFINITION),
                      CellType.STRING)
                  .setCellValue(definition.getDefinition());
            }
          }
          break;
        default:
          logger.error("Validation Type not found.");
          break;
      }
    }

    // The slots entries
    for (Slot slot : dataelement.getSlots()) {
      Sheet slotSheet = workbook.getSheet(Constants.NAME_SHEET_SLOTS);
      assignment = assignments.get(Constants.NAME_SHEET_SLOTS);

      Row slotRow = slotSheet.createRow(slotSheet.getLastRowNum() + 1);
      slotRow
          .createCell(
              assignment.get(Constants.Sheets.Slots.COLNAME_DATAELEMENT_ID), CellType.STRING)
          .setCellValue(Integer.toString(dataelement.getId()));
      slotRow
          .createCell(assignment.get(Constants.Sheets.Slots.COLNAME_KEY), CellType.STRING)
          .setCellValue(slot.getKey());
      slotRow
          .createCell(assignment.get(Constants.Sheets.Slots.COLNAME_VALUE), CellType.STRING)
          .setCellValue(slot.getValue());
    }

    // The conceptAssociations entries
    for (ConceptAssociation conceptAssociation : dataelement.getConceptAssociations()) {
      Sheet conceptAssociationSheet = workbook.getSheet(Constants.NAME_SHEET_CONCEPTASSOCIATION);
      assignment = assignments.get(Constants.NAME_SHEET_CONCEPTASSOCIATION);

      Row slotRow = conceptAssociationSheet.createRow(conceptAssociationSheet.getLastRowNum() + 1);
      slotRow
          .createCell(assignment.get(Constants.Sheets.ConceptAssociation.COLNAME_DATAELEMENT_ID),
              CellType.STRING)
          .setCellValue(Integer.toString(dataelement.getId()));
      slotRow
          .createCell(assignment.get(Sheets.ConceptAssociation.COLNAME_SYSTEM), CellType.STRING)
          .setCellValue(conceptAssociation.getSystem());
      slotRow
          .createCell(assignment.get(Sheets.ConceptAssociation.COLNAME_VERSION), CellType.STRING)
          .setCellValue(conceptAssociation.getVersion());
      slotRow
          .createCell(assignment.get(Sheets.ConceptAssociation.COLNAME_TERM), CellType.STRING)
          .setCellValue(conceptAssociation.getTerm());
      slotRow
          .createCell(assignment.get(Sheets.ConceptAssociation.COLNAME_TEXT), CellType.STRING)
          .setCellValue(conceptAssociation.getText());
      slotRow
          .createCell(assignment.get(Sheets.ConceptAssociation.COLNAME_LINKTYPE), CellType.STRING)
          .setCellValue(conceptAssociation.getLinktype().toString());
    }

    for (Dataelement child : dataelement.getChildren()) {
      addEntryToWorkbook(child);
    }
  }

  /** Create all necessary sheets as empty sheets with header rows. */
  private void initializeWorkbook() {
    workbook.createSheet(Constants.NAME_SHEET_INFO);
    Map<String, Integer> assignment;

    Sheet dataElementsSheet = workbook.createSheet(Constants.NAME_SHEET_DATAELEMENTS);
    assignment = Dataelement.createDefaultHeaderRow(dataElementsSheet);
    assignments.put(Constants.NAME_SHEET_DATAELEMENTS, assignment);

    Sheet definitionsSheet = workbook.createSheet(Constants.NAME_SHEET_DEFINITIONS);
    assignment = Definition.createDefaultHeaderRow(definitionsSheet);
    assignments.put(Constants.NAME_SHEET_DEFINITIONS, assignment);

    Sheet validationsSheetPermittedValues =
        workbook.createSheet(Constants.NAME_SHEET_VALIDATIONS_PERMITTEDVALUES);
    assignment = PermittedValuesValidation.createDefaultHeaderRow(validationsSheetPermittedValues);
    assignments.put(Constants.NAME_SHEET_VALIDATIONS_PERMITTEDVALUES, assignment);

    Sheet validationsSheetString = workbook.createSheet(Constants.NAME_SHEET_VALIDATIONS_STRING);
    assignment = StringValidation.createDefaultHeaderRow(validationsSheetString);
    assignments.put(Constants.NAME_SHEET_VALIDATIONS_STRING, assignment);

    Sheet validationsSheetFloat = workbook.createSheet(Constants.NAME_SHEET_VALIDATIONS_FLOAT);
    assignment = FloatValidation.createDefaultHeaderRow(validationsSheetFloat);
    assignments.put(Constants.NAME_SHEET_VALIDATIONS_FLOAT, assignment);

    Sheet validationsSheetInteger = workbook.createSheet(Constants.NAME_SHEET_VALIDATIONS_INTEGER);
    assignment = IntegerValidation.createDefaultHeaderRow(validationsSheetInteger);
    assignments.put(Constants.NAME_SHEET_VALIDATIONS_INTEGER, assignment);

    Sheet validationsSheetCalendar =
        workbook.createSheet(Constants.NAME_SHEET_VALIDATIONS_CALENDAR);
    assignment = CalendarValidation.createDefaultHeaderRow(validationsSheetCalendar);
    assignments.put(Constants.NAME_SHEET_VALIDATIONS_CALENDAR, assignment);

    Sheet slotsSheet = workbook.createSheet(Constants.NAME_SHEET_SLOTS);
    assignment = Slot.createDefaultHeaderRow(slotsSheet);
    assignments.put(Constants.NAME_SHEET_SLOTS, assignment);

    Sheet conceptAssociationsSheet = workbook.createSheet(Constants.NAME_SHEET_CONCEPTASSOCIATION);
    assignment = ConceptAssociation.createDefaultHeaderRow(conceptAssociationsSheet);
    assignments.put(Constants.NAME_SHEET_CONCEPTASSOCIATION, assignment);
  }

  private void createInfoSheet() {
    Sheet infoSheet = workbook.getSheet(Constants.NAME_SHEET_INFO);

    Row infoRow = infoSheet.createRow(0);
    infoRow
        .createCell(0, CellType.STRING)
        .setCellValue("This Excel sheet was automatically generated");

    Row namespaceRow = infoSheet.createRow(infoSheet.getLastRowNum() + 1);
    namespaceRow
        .createCell(0, CellType.STRING)
        .setCellValue(Constants.Sheets.Info.COLNAME_NAMESPACE);
    namespaceRow
        .createCell(1, CellType.STRING)
        .setCellValue("INSERT NAMESPACE NAME HERE. Otherwise default namespace is used.");
  }

  /**
   * Writes the workbook to an xml file on the filesystem.
   *
   * @param filename the filename to write to
   */
  public void writeToFile(String filename) throws IOException {
    FileOutputStream fileOut = new FileOutputStream(filename);
    workbook.write(fileOut);
    fileOut.close();
    workbook.close();
  }

  public Workbook getWorkbook() {
    return workbook;
  }
}
