/*
 * Copyright (C) 2018 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.staging.model.enums;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * The supported Date/Time format Strings for the MDR.
 *
 * <p>This contains ISO8601 and DIN5008 Date formats (with and without days), 24h and 12h time
 * formats (with and without seconds) as well as the reasonable combinations of date and time (date
 * withOUT days, but with a time is moot).
 */
public enum DateTimeFormat {

  /** Date only. */
  DATE_ISO8601_DAYS("yyyy-MM-dd"),
  DATE_ISO8601_NO_DAYS("yyyy-MM"),
  DATE_DIN5008_DAYS("dd.MM.yyyy"),
  DATE_DIN5008_NO_DAYS("MM.yyyy"),

  /** Time only. */
  TIME_24H_SECONDS("HH:mm:ss"),
  TIME_24H_NO_SECONDS("HH:mm"),
  TIME_12H_SECONDS("hh:mm:ss a"),
  TIME_12H_NO_SECONDS("hh:mm a"),

  /** Date and Time. */
  DATETIME_ISO8601_DAYS_24H_SECONDS(
      DATE_ISO8601_DAYS.getFormatString() + " " + TIME_24H_SECONDS.getFormatString()),
  DATETIME_ISO8601_DAYS_24H_NO_SECONDS(
      DATE_ISO8601_DAYS.getFormatString() + " " + TIME_24H_NO_SECONDS.getFormatString()),
  DATETIME_ISO8601_DAYS_12H_SECONDS(
      DATE_ISO8601_DAYS.getFormatString() + " " + TIME_12H_SECONDS.getFormatString()),
  DATETIME_ISO8601_DAYS_12H_NO_SECONDS(
      DATE_ISO8601_DAYS.getFormatString() + " " + TIME_12H_NO_SECONDS.getFormatString()),

  DATETIME_DIN5008_DAYS_24H_SECONDS(
      DATE_DIN5008_DAYS.getFormatString() + " " + TIME_24H_SECONDS.getFormatString()),
  DATETIME_DIN5008_DAYS_24H_NO_SECONDS(
      DATE_DIN5008_DAYS.getFormatString() + " " + TIME_24H_NO_SECONDS.getFormatString()),
  DATETIME_DIN5008_DAYS_12H_SECONDS(
      DATE_DIN5008_DAYS.getFormatString() + " " + TIME_12H_SECONDS.getFormatString()),
  DATETIME_DIN5008_DAYS_12H_NO_SECONDS(
      DATE_DIN5008_DAYS.getFormatString() + " " + TIME_12H_NO_SECONDS.getFormatString());

  private final SimpleDateFormat dateFormat;

  DateTimeFormat(String dateFormatString) {
    dateFormat = new SimpleDateFormat(dateFormatString);
  }

  /**
   * Create DateTimeFormat from the string representation.
   *
   * @param dateFormatString String representation of the DateFormat
   * @return DateTimeFormat instance or null if no matching format was found
   */
  public static DateTimeFormat fromString(String dateFormatString) {
    for (DateTimeFormat df : DateTimeFormat.values()) {
      if (df.getFormatString().equals(dateFormatString)) {
        return df;
      }
    }
    return null;
  }

  public DateFormat getFormat() {
    return dateFormat;
  }

  public String getFormatString() {
    return dateFormat.toPattern();
  }

  /**
   * Check if this instance is a date.
   *
   * @return true for all date-only formats
   */
  public boolean isDate() {
    return this == DATE_ISO8601_DAYS
        || this == DATE_ISO8601_NO_DAYS
        || this == DATE_DIN5008_DAYS
        || this == DATE_DIN5008_NO_DAYS;
  }

  /**
   * Check if this instance is a time.
   *
   * @return true for all time-only formats
   */
  public boolean isTime() {
    return this == TIME_24H_SECONDS
        || this == TIME_24H_NO_SECONDS
        || this == TIME_12H_SECONDS
        || this == TIME_12H_NO_SECONDS;
  }

  /**
   * Check if this instance contains days.
   *
   * @return true for all formats that contain days
   */
  public boolean isWithDays() {
    return isDateTime() || this == DATE_ISO8601_DAYS || this == DATE_DIN5008_DAYS;
  }

  /**
   * Check if this instance contains seconds.
   *
   * @return true for all formats that contain seconds
   */
  public boolean isWithSeconds() {
    return this == TIME_24H_SECONDS
        || this == TIME_12H_SECONDS
        || this == DATETIME_ISO8601_DAYS_24H_SECONDS
        || this == DATETIME_ISO8601_DAYS_12H_SECONDS
        || this == DATETIME_DIN5008_DAYS_24H_SECONDS
        || this == DATETIME_DIN5008_DAYS_12H_SECONDS;
  }

  /**
   * Check if this instance contains date and time.
   *
   * @return true for all formats that contain date and time
   */
  public boolean isDateTime() {
    return !isDate() && !isTime();
  }
}
