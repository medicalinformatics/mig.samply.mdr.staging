/*
 * Copyright (C) 2018 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.staging.model.adapter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.TypeAdapter;
import com.google.gson.internal.LazilyParsedNumber;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import de.samply.mdr.xsd.staging.BooleanValidation;
import de.samply.mdr.xsd.staging.CalendarValidation;
import de.samply.mdr.xsd.staging.CatalogValidation;
import de.samply.mdr.xsd.staging.FloatValidation;
import de.samply.mdr.xsd.staging.IntegerValidation;
import de.samply.mdr.xsd.staging.PermittedValuesValidation;
import de.samply.mdr.xsd.staging.StringValidation;
import de.samply.mdr.xsd.staging.TbdValidation;
import de.samply.mdr.xsd.staging.ValidationType;
import de.samply.mdr.xsd.staging.ValidationTypeEnum;
import de.samply.string.util.StringUtil;
import java.io.IOException;

/**
 * TypeAdapter Class to handle ValidationType and its extensions.
 *
 * <p>When reading a validation object, check the validationType attribute and parse the JSON Object
 * with the proper validation class.
 */
public class ValidationTypeAdapter extends TypeAdapter<ValidationType> {

  private static Gson GSON;

  static {
    GsonBuilder gsonBuilder = new GsonBuilder();
    gsonBuilder.disableHtmlEscaping();
    GSON = gsonBuilder.create();
  }

  public ValidationTypeAdapter() {}

  /**
   * Writes one JSON value (an array, object, string, number, boolean or null) for validation.
   *
   * @param jsonWriter the JsonWriter to use
   * @param validation the ValidationType to write
   */
  @Override
  public void write(JsonWriter jsonWriter, ValidationType validation) throws IOException {
    if (validation == null) {
      jsonWriter.nullValue();
      return;
    }
    jsonWriter.jsonValue(GSON.toJson(validation));
  }

  /**
   * Reads one JSON value (an array, object, string, number, boolean or null) and converts it to a
   * ValidationType object. Returns the converted object.
   *
   * @param jsonReader the JsonReader to use
   * @return the converted ValidationType object. May be null.
   */
  @Override
  public ValidationType read(JsonReader jsonReader) throws IOException {
    if (jsonReader.peek() == JsonToken.NULL) {
      jsonReader.nextNull();
      return null;
    } else {
      JsonElement jsonElement = readJsonElement(jsonReader);
      JsonObject jsonObject = jsonElement.getAsJsonObject();
      String validationTypeString = jsonObject.getAsJsonPrimitive("validationType").getAsString();

      if (StringUtil.isEmpty(validationTypeString)) {
        return null;
      } else {
        ValidationTypeEnum validationTypeEnum = ValidationTypeEnum.valueOf(validationTypeString);
        switch (validationTypeEnum) {
          case BOOLEAN_VALIDATION:
            return GSON.fromJson(jsonObject, BooleanValidation.class);
          case CALENDAR_VALIDATION:
            return GSON.fromJson(jsonObject, CalendarValidation.class);
          case CATALOG_VALIDATION:
            return GSON.fromJson(jsonObject, CatalogValidation.class);
          case FLOAT_VALIDATION:
            return GSON.fromJson(jsonObject, FloatValidation.class);
          case INTEGER_VALIDATION:
            return GSON.fromJson(jsonObject, IntegerValidation.class);
          case PERMITTED_VALUES_VALIDATION:
            return GSON.fromJson(jsonObject, PermittedValuesValidation.class);
          case STRING_VALIDATION:
            return GSON.fromJson(jsonObject, StringValidation.class);
          case TBD_VALIDATION:
            return GSON.fromJson(jsonObject, TbdValidation.class);
          default:
            return null;
        }
      }
    }
  }

  /**
   * Read the next element from a json reader.
   *
   * @param in the jsonreader to read from
   * @return The next element - either a jsonprimitive, jsonobject or jsonarray
   */
  private JsonElement readJsonElement(JsonReader in) throws IOException {
    switch (in.peek()) {
      case NUMBER:
        return new JsonPrimitive(new LazilyParsedNumber(in.nextString()));
      case BOOLEAN:
        return new JsonPrimitive(in.nextBoolean());
      case STRING:
        return new JsonPrimitive(in.nextString());
      case NULL:
        in.nextNull();
        return JsonNull.INSTANCE;
      case BEGIN_ARRAY:
        JsonArray array = new JsonArray();
        in.beginArray();
        while (in.hasNext()) {
          array.add(readJsonElement(in));
        }
        in.endArray();
        return array;
      case BEGIN_OBJECT:
        JsonObject object = new JsonObject();
        in.beginObject();
        while (in.hasNext()) {
          object.add(in.nextName(), readJsonElement(in));
        }
        in.endObject();
        return object;
      default:
        throw new IllegalArgumentException();
    }
  }
}
