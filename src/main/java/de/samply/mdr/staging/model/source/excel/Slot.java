/*
 * Copyright (C) 2018 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.staging.model.source.excel;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import de.samply.mdr.staging.Constants;
import de.samply.mdr.staging.utils.ExcelUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Represents one Slot entry in an excel workbook. */
public class Slot {

  private static final Logger logger = LoggerFactory.getLogger(Slot.class);

  private int dataElementId;
  private String key;
  private String value;

  /**
   * Read all entries from the slots sheet into a list.
   *
   * @param workbook the workbook, containing the slots sheet
   * @return a list containing all slots
   */
  public static List<Slot> readListFromWorkbook(Workbook workbook) {
    List<Slot> slots = new ArrayList<>();

    Sheet sheet = workbook.getSheet(Constants.Sheets.Slots.SHEETNAME);

    if (sheet == null) {
      return slots;
    }

    Row headerRow = sheet.getRow(Constants.ROW_INDEX_HEADER);
    Map<String, Integer> columnAssignments = ExcelUtil.getColumnAssignmentsFromHeader(headerRow);

    Iterator<Row> rowIterator = sheet.rowIterator();

    if (rowIterator.hasNext()) {
      rowIterator.next(); // skip header row
    }

    while (rowIterator.hasNext()) {
      Row row = rowIterator.next();
      if (row.getPhysicalNumberOfCells() > 0) {
        try {
          Slot slot = new Slot();
          slot.setDataElementId(
              ExcelUtil.getCellValueAsInteger(
                  row.getCell(
                      columnAssignments.get(Constants.Sheets.Slots.COLNAME_DATAELEMENT_ID))));
          slot.setKey(
              ExcelUtil.getCellValueAsString(
                  row.getCell(columnAssignments.get(Constants.Sheets.Slots.COLNAME_KEY))));
          slot.setValue(
              ExcelUtil.getCellValueAsString(
                  row.getCell(columnAssignments.get(Constants.Sheets.Slots.COLNAME_VALUE))));
          slots.add(slot);
        } catch (NullPointerException npe) {
          logger.debug(
              "Caught null pointer exception while trying to read slot. Skipping this one...");
        }
      }
    }

    return slots;
  }

  /**
   * Read all entries from the slots sheet into a multimap.
   *
   * <p>This contains a certain redundancy (dataelement id is saved twice) but allows for more
   * convenient access.
   *
   * @param workbook the workbook, containing the slots sheet
   * @return a multimap with the assignment elementid->slots
   */
  public static Multimap<Integer, Slot> readMultiMapFromWorkbook(Workbook workbook) {
    Multimap<Integer, Slot> slots = ArrayListMultimap.create();

    Sheet sheet = workbook.getSheet(Constants.Sheets.Slots.SHEETNAME);

    if (sheet == null) {
      return slots;
    }

    Row headerRow = sheet.getRow(Constants.ROW_INDEX_HEADER);
    Map<String, Integer> columnAssignments = ExcelUtil.getColumnAssignmentsFromHeader(headerRow);

    Iterator<Row> rowIterator = sheet.rowIterator();

    if (rowIterator.hasNext()) {
      rowIterator.next(); // skip header row
    }

    while (rowIterator.hasNext()) {
      Row row = rowIterator.next();
      if (row.getPhysicalNumberOfCells() > 0) {
        try {
          Slot slot = new Slot();
          slot.setDataElementId(
              ExcelUtil.getCellValueAsInteger(
                  row.getCell(
                      columnAssignments.get(Constants.Sheets.Slots.COLNAME_DATAELEMENT_ID))));
          slot.setKey(
              ExcelUtil.getCellValueAsString(
                  row.getCell(columnAssignments.get(Constants.Sheets.Slots.COLNAME_KEY))));
          slot.setValue(
              ExcelUtil.getCellValueAsString(
                  row.getCell(columnAssignments.get(Constants.Sheets.Slots.COLNAME_VALUE))));
          slots.put(slot.getDataElementId(), slot);
        } catch (Exception npe) {
          logger.debug(
              "Caught exception in line {} while trying to read slot. Skipping this row.",
              row.getRowNum());
        }
      }
    }

    return slots;
  }

  /**
   * Create the header row and return the column assignment.
   *
   * @param sheet the xml sheet in which the rows shall be created
   * @return a map containing the assignment from header name to cell position
   */
  public static Map<String, Integer> createDefaultHeaderRow(Sheet sheet) {
    Map<String, Integer> assignment = new HashMap<>();
    int cellIndex = 0;

    Row headerRow = sheet.createRow(cellIndex);
    headerRow
        .createCell(cellIndex, CellType.STRING)
        .setCellValue(Constants.Sheets.Slots.COLNAME_DATAELEMENT_ID);
    assignment.put(Constants.Sheets.Slots.COLNAME_DATAELEMENT_ID, cellIndex);
    ++cellIndex;

    headerRow
        .createCell(cellIndex, CellType.STRING)
        .setCellValue(Constants.Sheets.Slots.COLNAME_KEY);
    assignment.put(Constants.Sheets.Slots.COLNAME_KEY, cellIndex);
    ++cellIndex;

    headerRow
        .createCell(cellIndex, CellType.STRING)
        .setCellValue(Constants.Sheets.Slots.COLNAME_VALUE);
    assignment.put(Constants.Sheets.Slots.COLNAME_VALUE, cellIndex);

    return assignment;
  }

  public int getDataElementId() {
    return dataElementId;
  }

  public void setDataElementId(int dataElementId) {
    this.dataElementId = dataElementId;
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  @Override
  public int hashCode() {
    return super.hashCode();
  }

  @Override
  public boolean equals(Object o) {
    return super.equals(o);
  }

  @Override
  public String toString() {
    return "Slot{"
        + "dataElementId="
        + dataElementId
        + ", key='"
        + key
        + '\''
        + ", value='"
        + value
        + '\''
        + '}';
  }
}
