/*
 * Copyright (C) 2020 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.staging.model.enums;

/** The different relation types allowed in the mdr. */
public enum RelationType {
  RT_UNDEFINED("undefined"),
  RT_EQUAL("equal"),
  RT_EQUIVALENT("equivalent"),
  RT_WIDER("wider"),
  RT_SUBSUMES("subsumes"),
  RT_NARROWER("narrower"),
  RT_SPECIALIZES("specializes"),
  RT_INEXACT("inexact");


  private static final String SHEET_PREFIX = "linktype_";
  private final String linktypeName;

  RelationType(String linktypeName) {
    this.linktypeName = linktypeName;
  }

  /**
   * TODO: add javadoc.
   */
  public static RelationType fromString(String linktypeName) {
    for (RelationType type : RelationType.values()) {
      if (type.getLinktypeName().equalsIgnoreCase(linktypeName)) {
        return type;
      }
    }
    return null;
  }

  public String getLinktypeName() {
    return linktypeName;
  }

  public String getSheetName() {
    return SHEET_PREFIX + linktypeName;
  }

}
