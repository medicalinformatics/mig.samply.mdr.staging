/*
 * Copyright (C) 2018 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.staging.model.source.excel;

import de.samply.mdr.staging.Constants;
import java.util.Iterator;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Represents the info sheet of an excel workbook. */
public class Info {

  private static final Logger logger = LoggerFactory.getLogger(Info.class);

  private String namespaceName;

  private String importLabel;

  /**
   * Read the info sheet.
   *
   * @param workbook the workbook to read from
   * @return the info contained on the page
   */
  public static Info readFromWorkbook(Workbook workbook) {
    Info info = new Info();

    Sheet sheet = workbook.getSheet(Constants.Sheets.Info.SHEETNAME);

    if (sheet == null) {
      return info;
    }

    Iterator<Row> rowIterator = sheet.rowIterator();

    while (rowIterator.hasNext()) {
      Row row = rowIterator.next();
      if (row.getPhysicalNumberOfCells() > 0) {
        try {
          Cell cell = row.getCell(0);
          String key = cell.getStringCellValue();
          if (Constants.Sheets.Info.COLNAME_NAMESPACE.equalsIgnoreCase(key)) {
            info.setNamespaceName(row.getCell(1).getStringCellValue());
          }
          if (Constants.Sheets.Info.COLNAME_IMPORT_LABEL.equalsIgnoreCase(key)) {
            info.setImportLabel(row.getCell(1).getStringCellValue());
          }
        } catch (Exception npe) {
          logger.debug(
              "Caught exception in line {} while trying to read info sheet. Skipping this row.",
              row.getRowNum());
        }
      }
    }

    return info;
  }

  public String getNamespaceName() {
    return namespaceName;
  }

  public void setNamespaceName(String namespaceName) {
    this.namespaceName = namespaceName;
  }

  public String getImportLabel() {
    return importLabel;
  }

  public void setImportLabel(String importLabel) {
    this.importLabel = importLabel;
  }

  @Override
  public String toString() {
    return "Info{"
        + "namespaceName='"
        + namespaceName
        + '\''
        + ", importLabel='"
        + importLabel
        + '\''
        + '}';
  }
}
