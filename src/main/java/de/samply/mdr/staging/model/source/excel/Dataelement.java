/*
 * Copyright (C) 2018 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.staging.model.source.excel;

import com.google.common.collect.Multimap;
import de.samply.mdr.lib.Constants.Language;
import de.samply.mdr.staging.Constants;
import de.samply.mdr.staging.model.enums.ValidationType;
import de.samply.mdr.staging.model.validators.BooleanValidation;
import de.samply.mdr.staging.model.validators.FloatValidation;
import de.samply.mdr.staging.model.validators.IntegerValidation;
import de.samply.mdr.staging.model.validators.StringValidation;
import de.samply.mdr.staging.model.validators.TbdValidation;
import de.samply.mdr.staging.utils.ExcelUtil;
import de.samply.mdr.xsd.staging.ElementType;
import de.samply.string.util.StringUtil;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Represents a dataelement (or group or record) in an excel workbook. */
public class Dataelement {

  private static final Logger logger = LoggerFactory.getLogger(Dataelement.class);

  private int id;
  private Integer parentId;
  private ElementType elementType;
  private List<Dataelement> children;
  private List<Slot> slots;
  private List<Definition> definitions;
  private Validation validation;
  private List<ConceptAssociation> conceptAssociations;

  /**
   * TODO: add javadoc.
   */
  public Dataelement(ElementType elementType) {
    this.elementType = elementType;
    children = new ArrayList<>();
    slots = new ArrayList<>();
    definitions = new ArrayList<>();
    conceptAssociations = new ArrayList<>();
  }

  /**
   * Read the dataelements from a workbook to a list.
   *
   * @param workbook the workbook that contains the dataelements
   * @param definitions a multimap with all definitions in the workbook
   * @param validations a map with all validations in the workbook
   * @param slots a multimap with all slots in the workbook
   * @param conceptAssociations a multimap with all conceptAssociations in the workbook
   * @return a list with all dataelements (as well as groups and records)
   */
  public static List<Dataelement> readFromWorkbook(
      Workbook workbook,
      // ElementType elementType,
      Multimap<Integer, Definition> definitions,
      Map<AbstractMap.SimpleEntry<ValidationType, Integer>, Validation> validations,
      Multimap<Integer, Slot> slots,
      Multimap<Integer, ConceptAssociation> conceptAssociations) {

    List<Dataelement> dataelements = new ArrayList<>();

    Sheet sheet = workbook.getSheet(Constants.Sheets.Dataelements.SHEETNAME);

    if (sheet == null) {
      return dataelements;
    }

    Row headerRow = sheet.getRow(Constants.ROW_INDEX_HEADER);
    Map<String, Integer> columnAssignments = ExcelUtil.getColumnAssignmentsFromHeader(headerRow);

    Iterator<Row> rowIterator = sheet.rowIterator();

    if (rowIterator.hasNext()) {
      rowIterator.next(); // skip header row
    }

    while (rowIterator.hasNext()) {
      Row row = rowIterator.next();
      if (row.getPhysicalNumberOfCells() > 0) {
        try {
          Dataelement dataelement;
          try {
            dataelement =
                new Dataelement(
                    ElementType.fromValue(
                        ExcelUtil.getCellValueAsString(
                            row.getCell(
                                columnAssignments.get(
                                    Constants.Sheets.Dataelements.COLNAME_ELEMENT_TYPE)))));
          } catch (IllegalArgumentException e) {
            continue;
          }

          // Check if the "id" column is set. If it isn't, read the temp_id column.
          Integer id;
          try {
            id =
                ExcelUtil.getCellValueAsInteger(
                    row.getCell(columnAssignments.get(Constants.Sheets.Dataelements.COLNAME_ID)));
          } catch (Exception e) {
            id = null;
          }
          if (id == null || id < 0) {
            id =
                ExcelUtil.getCellValueAsInteger(
                    row.getCell(
                        columnAssignments.get(Constants.Sheets.Dataelements.COLNAME_TEMP_ID)));
          }

          dataelement.setId(id);
          dataelement.setParentId(
              ExcelUtil.getCellValueAsInteger(
                  row.getCell(
                      columnAssignments.get(Constants.Sheets.Dataelements.COLNAME_PARENT_ID))));
          dataelement
              .getDefinitions()
              .addAll(
                  definitions.get(
                      ExcelUtil.getCellValueAsInteger(
                          row.getCell(
                              columnAssignments.get(
                                  Constants.Sheets.Dataelements.COLNAME_DEFINITION_ID)))));

          dataelement.getSlots().addAll(slots.get(dataelement.getId()));
          dataelement.getConceptAssociations().addAll(conceptAssociations.get(dataelement.getId()));
          Validation val;
          ValidationType validationType =
              ValidationType.fromString(
                  ExcelUtil.getCellValueAsString(
                      row.getCell(
                          columnAssignments.get(
                              Constants.Sheets.Dataelements.COLNAME_VALIDATION_TYPE))));

          if (validationType == ValidationType.VT_BOOLEAN) {
            // Boolean validations have no separate sheets as they have no parameters. Just create a
            // new object.
            val = new BooleanValidation();
          } else {
            val =
                validations.get(
                    new AbstractMap.SimpleEntry<>(
                        validationType,
                        ExcelUtil.getCellValueAsInteger(
                            row.getCell(
                                columnAssignments.get(
                                    Constants.Sheets.Dataelements.COLNAME_VALIDATION_ID)))));
          }

          // If there was no validation found with this type and id, or if no id was specified,
          // use a default validation with no constraints for String (no max length, no regex) and
          // numerical (float, integer) validations (no min/max value). Otherwise use TBD validation
          if (dataelement.getElementType() == ElementType.DATAELEMENT && val == null) {
            switch (validationType) {
              case VT_FLOAT:
                val = new FloatValidation();
                break;
              case VT_INTEGER:
                val = new IntegerValidation();
                break;
              case VT_STRING:
                val = new StringValidation();
                break;
              default:
                val = new TbdValidation();
                break;
            }
          }
          dataelement.setValidation(val);
          dataelements.add(dataelement);
        } catch (NullPointerException npe) {
          logger.debug("Caught null pointer exception while trying to read data element. "
                  + "Skipping this one...", npe);
        }
      }
    }

    return dataelements;
  }

  /**
   * Create the header row and return the column assignment.
   *
   * @param sheet the xml sheet in which the rows shall be created
   * @return a map containing the assignment from header name to cell position
   */
  public static Map<String, Integer> createDefaultHeaderRow(Sheet sheet) {
    Map<String, Integer> assignment = new HashMap<>();
    int cellIndex = 0;

    Row headerRow = sheet.createRow(cellIndex);
    headerRow
        .createCell(cellIndex, CellType.STRING)
        .setCellValue(Constants.Sheets.Dataelements.COLNAME_ID);
    assignment.put(Constants.Sheets.Dataelements.COLNAME_ID, cellIndex);
    ++cellIndex;

    headerRow
        .createCell(cellIndex, CellType.STRING)
        .setCellValue(Constants.Sheets.Dataelements.COLNAME_PARENT_ID);
    assignment.put(Constants.Sheets.Dataelements.COLNAME_PARENT_ID, cellIndex);
    ++cellIndex;

    headerRow
        .createCell(cellIndex, CellType.STRING)
        .setCellValue(Constants.Sheets.Dataelements.COLNAME_ELEMENT_TYPE);
    assignment.put(Constants.Sheets.Dataelements.COLNAME_ELEMENT_TYPE, cellIndex);
    ++cellIndex;

    headerRow
        .createCell(cellIndex, CellType.STRING)
        .setCellValue(Constants.Sheets.Dataelements.COLNAME_DEFINITION_ID);
    assignment.put(Constants.Sheets.Dataelements.COLNAME_DEFINITION_ID, cellIndex);
    ++cellIndex;

    headerRow
        .createCell(cellIndex, CellType.STRING)
        .setCellValue(Constants.Sheets.Dataelements.COLNAME_VALIDATION_TYPE);
    assignment.put(Constants.Sheets.Dataelements.COLNAME_VALIDATION_TYPE, cellIndex);
    ++cellIndex;

    headerRow
        .createCell(cellIndex, CellType.STRING)
        .setCellValue(Constants.Sheets.Dataelements.COLNAME_VALIDATION_ID);
    assignment.put(Constants.Sheets.Dataelements.COLNAME_VALIDATION_ID, cellIndex);

    return assignment;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Integer getParentId() {
    return parentId;
  }

  public void setParentId(Integer parentId) {
    this.parentId = parentId;
  }

  public ElementType getElementType() {
    return elementType;
  }

  public void setElementType(ElementType elementType) {
    this.elementType = elementType;
  }

  public List<Dataelement> getChildren() {
    return children;
  }

  public void setChildren(List<Dataelement> children) {
    this.children = children;
  }

  public List<Slot> getSlots() {
    return slots;
  }

  public void setSlots(List<Slot> slots) {
    this.slots = slots;
  }

  public List<Definition> getDefinitions() {
    return definitions;
  }

  public void setDefinitions(List<Definition> definitions) {
    this.definitions = definitions;
  }

  public Validation getValidation() {
    return validation;
  }

  public void setValidation(Validation validation) {
    this.validation = validation;
  }

  public List<ConceptAssociation> getConceptAssociations() {
    return conceptAssociations;
  }

  public void setConceptAssociations(List<ConceptAssociation> conceptAssociations) {
    this.conceptAssociations = conceptAssociations;
  }

  @Override
  public String toString() {
    return "Dataelement{"
        + "id="
        + id
        + ", parentId="
        + parentId
        + ", elementType="
        + elementType
        + ", children="
        + children
        + ", slots="
        + slots
        + ", conceptAssociations="
        + conceptAssociations
        + ", definitions="
        + definitions
        + ", validation="
        + validation
        + '}';
  }

  /**
   * If a language is submitted, try to return the definition in this language, return the first one
   * otherwise.
   */
  public Definition getPrioritizedDefinition(Language language) {
    if (language != null && !StringUtil.isEmpty(language.getName())) {
      for (Definition def : definitions) {
        if (language.equals(def.getLanguage())) {
          return def;
        }
      }
    }
    return definitions.get(0);
  }

  public Definition getPrioritizedDefinition() {
    return getPrioritizedDefinition(null);
  }
}
