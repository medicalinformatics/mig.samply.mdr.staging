/*
 * Copyright (C) 2018 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.staging.model.source.excel;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import de.samply.mdr.lib.Constants.Language;
import de.samply.mdr.staging.Constants;
import de.samply.mdr.staging.utils.ExcelUtil;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Represents one Definition entry in an excel workbook. */
public class Definition {

  private static final Logger logger = LoggerFactory.getLogger(Definition.class);

  private String designation;
  private String definition;
  private Language language;

  /**
   * Read all available definitions to a multimap.
   *
   * <p>this will contain all available languages.
   *
   * @param workbook the whole xml workbook
   * @return a multimap with all definitions
   */
  public static Multimap<Integer, Definition> readFromWorkbook(Workbook workbook) {
    Multimap<Integer, Definition> definitions = ArrayListMultimap.create();

    Sheet sheet = workbook.getSheet(Constants.Sheets.Definitions.SHEETNAME);

    if (sheet == null) {
      return definitions;
    }

    Row headerRow = sheet.getRow(Constants.ROW_INDEX_HEADER);
    Map<String, Integer> columnAssignments = ExcelUtil.getColumnAssignmentsFromHeader(headerRow);

    Iterator<Row> rowIterator = sheet.rowIterator();

    if (rowIterator.hasNext()) {
      rowIterator.next(); // skip header row
    }

    while (rowIterator.hasNext()) {
      Row row = rowIterator.next();
      if (row.getPhysicalNumberOfCells() > 0) {
        try {
          Definition definition = new Definition();
          definition.setDefinition(
              ExcelUtil.getCellValueAsString(
                  row.getCell(
                      columnAssignments.get(Constants.Sheets.Definitions.COLNAME_DEFINITION))));
          definition.setDesignation(
              ExcelUtil.getCellValueAsString(
                  row.getCell(
                      columnAssignments.get(Constants.Sheets.Definitions.COLNAME_DESIGNATION))));
          String languageString =
              ExcelUtil.getCellValueAsString(
                  row.getCell(
                      columnAssignments.get(Constants.Sheets.Definitions.COLNAME_LANGUAGE)));
          definition.setLanguage(Language.valueOf(languageString.toUpperCase()));
          int definitionId =
              ExcelUtil.getCellValueAsInteger(
                  row.getCell(columnAssignments.get(Constants.Sheets.Definitions.COLNAME_ID)));
          definitions.put(definitionId, definition);
        } catch (Exception e) {
          logger.debug(
              "Caught exception in line {} while trying to read definition. Skipping this row.",
              row.getRowNum());
        }
      }
    }

    return definitions;
  }

  /**
   * Create the header row and return the column assignment.
   *
   * @param sheet the xml sheet in which the rows shall be created
   * @return a map containing the assignment from header name to cell position
   */
  public static Map<String, Integer> createDefaultHeaderRow(Sheet sheet) {
    Map<String, Integer> assignment = new HashMap<>();
    int cellIndex = 0;

    Row headerRow = sheet.createRow(cellIndex);
    headerRow
        .createCell(cellIndex, CellType.STRING)
        .setCellValue(Constants.Sheets.Definitions.COLNAME_ID);
    assignment.put(Constants.Sheets.Definitions.COLNAME_ID, cellIndex);
    ++cellIndex;

    headerRow
        .createCell(cellIndex, CellType.STRING)
        .setCellValue(Constants.Sheets.Definitions.COLNAME_LANGUAGE);
    assignment.put(Constants.Sheets.Definitions.COLNAME_LANGUAGE, cellIndex);
    ++cellIndex;

    headerRow
        .createCell(cellIndex, CellType.STRING)
        .setCellValue(Constants.Sheets.Definitions.COLNAME_DESIGNATION);
    assignment.put(Constants.Sheets.Definitions.COLNAME_DESIGNATION, cellIndex);
    ++cellIndex;

    headerRow
        .createCell(cellIndex, CellType.STRING)
        .setCellValue(Constants.Sheets.Definitions.COLNAME_DEFINITION);
    assignment.put(Constants.Sheets.Definitions.COLNAME_DEFINITION, cellIndex);

    return assignment;
  }

  public String getDesignation() {
    return designation;
  }

  public void setDesignation(String designation) {
    this.designation = designation;
  }

  public String getDefinition() {
    return definition;
  }

  public void setDefinition(String definition) {
    this.definition = definition;
  }

  public Language getLanguage() {
    return language;
  }

  public void setLanguage(Language language) {
    this.language = language;
  }

  @Override
  public String toString() {
    return "Definition{"
        + "designation='"
        + designation
        + '\''
        + ", definition='"
        + definition
        + '\''
        + ", language="
        + language
        + '}';
  }
}
