/*
 * Copyright (C) 2018 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.staging.model.source.excel;

import com.google.common.collect.Multimap;
import com.google.gson.annotations.JsonAdapter;
import de.samply.mdr.staging.model.adapter.ValidationTypeAdapter;
import de.samply.mdr.staging.model.enums.ValidationType;
import de.samply.mdr.staging.model.validators.CalendarValidation;
import de.samply.mdr.staging.model.validators.CatalogValidation;
import de.samply.mdr.staging.model.validators.FloatValidation;
import de.samply.mdr.staging.model.validators.IntegerValidation;
import de.samply.mdr.staging.model.validators.PermittedValuesValidation;
import de.samply.mdr.staging.model.validators.StringValidation;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Represents one Validation entry in an excel workbook. */
@JsonAdapter(ValidationTypeAdapter.class)
public abstract class Validation {

  protected static final Logger logger = LoggerFactory.getLogger(Validation.class);

  private ValidationType validationType;
  private int id;

  /**
   * Read and combine all validation sheets.
   *
   * @param workbook the workbook to read from
   * @param definitions the definitions found in the workbook (needed for Permitted Value lists)
   * @return a map containing all validation entries in the workbook
   */
  public static Map<AbstractMap.SimpleEntry<ValidationType, Integer>, Validation>
      readAllFromWorkbook(Workbook workbook, Multimap<Integer, Definition> definitions) {
    Map<AbstractMap.SimpleEntry<ValidationType, Integer>, Validation> validations = new HashMap<>();

    validations.putAll(StringValidation.readFromWorkbook(workbook));
    validations.putAll(FloatValidation.readFromWorkbook(workbook));
    validations.putAll(IntegerValidation.readFromWorkbook(workbook));
    validations.putAll(CalendarValidation.readFromWorkbook(workbook));
    validations.putAll(CatalogValidation.readFromWorkbook(workbook));
    validations.putAll(PermittedValuesValidation.readFromWorkbook(workbook, definitions));

    return validations;
  }

  public ValidationType getValidationType() {
    return validationType;
  }

  public void setValidationType(ValidationType validationType) {
    this.validationType = validationType;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }
}
