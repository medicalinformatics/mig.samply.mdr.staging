/*
 * Copyright (C) 2018 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.staging.model.validators;

import de.samply.mdr.staging.Constants;
import de.samply.mdr.staging.model.enums.ValidationType;
import de.samply.mdr.staging.model.source.excel.Validation;
import de.samply.mdr.staging.utils.ExcelUtil;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * Float Validation.
 *
 * <p>Contains upper and lower bound as well as unit of measure.
 */
public class FloatValidation extends Validation {

  private boolean withinRange;
  private Double rangeFrom;
  private Double rangeTo;
  private String unitOfMeasure;

  public FloatValidation() {
    setValidationType(ValidationType.VT_FLOAT);
  }

  /**
   * Read the Float Validations from the given workbook.
   *
   * @param workbook the workbook to read from
   * @return a map containing the validationtype and id as key and the validation itself as value
   */
  public static Map<AbstractMap.SimpleEntry<ValidationType, Integer>, Validation> readFromWorkbook(
      Workbook workbook) {
    Map<AbstractMap.SimpleEntry<ValidationType, Integer>, Validation> validations = new HashMap<>();

    Sheet sheet =
        workbook.getSheet(
            Constants.Sheets.Validations.ValidationsNumeric.ValidationsFloat.SHEETNAME);

    if (sheet == null) {
      return validations;
    }

    Row headerRow = sheet.getRow(Constants.ROW_INDEX_HEADER);
    Map<String, Integer> columnAssignments = ExcelUtil.getColumnAssignmentsFromHeader(headerRow);

    Iterator<Row> rowIterator = sheet.rowIterator();

    if (rowIterator.hasNext()) {
      rowIterator.next(); // skip header row
    }

    while (rowIterator.hasNext()) {
      Row row = rowIterator.next();
      if (row.getPhysicalNumberOfCells() > 0) {
        try {
          FloatValidation validation = new FloatValidation();
          validation.setId(
              ExcelUtil.getCellValueAsInteger(
                  row.getCell(columnAssignments.get(Constants.Sheets.Validations.COLNAME_ID))));

          validation.setRangeFrom(
              ExcelUtil.getCellValueAsDouble(
                  row.getCell(
                      columnAssignments.get(
                          Constants.Sheets.Validations.ValidationsNumeric.COLNAME_RANGE_FROM))));
          validation.setRangeTo(
              ExcelUtil.getCellValueAsDouble(
                  row.getCell(
                      columnAssignments.get(
                          Constants.Sheets.Validations.ValidationsNumeric.COLNAME_RANGE_TO))));
          validation.setUnitOfMeasure(
              ExcelUtil.getCellValueAsString(
                  row.getCell(
                      columnAssignments.get(
                          Constants.Sheets.Validations.ValidationsNumeric
                              .COLNAME_UNIT_OF_MEASURE))));
          validation.setWithinRange(validation.rangeFrom != null || validation.rangeTo != null);

          AbstractMap.SimpleEntry<ValidationType, Integer> mapKey =
              new AbstractMap.SimpleEntry<>(
                  ValidationType.VT_FLOAT,
                  ExcelUtil.getCellValueAsInteger(
                      row.getCell(columnAssignments.get(Constants.Sheets.Validations.COLNAME_ID))));
          validations.put(mapKey, validation);
        } catch (Exception e) {
          logger.debug(
              "Caught exception in line {} while trying to read float validation. "
                  + "Skipping this row.",
              row.getRowNum());
        }
      }
    }

    return validations;
  }

  /**
   * Create the header row and return the column assignment.
   *
   * @param sheet the xml sheet in which the rows shall be created
   * @return a map containing the assignment from header name to cell position
   */
  public static Map<String, Integer> createDefaultHeaderRow(Sheet sheet) {
    Map<String, Integer> assignment = new HashMap<>();
    int cellIndex = 0;

    Row headerRow = sheet.createRow(cellIndex);
    headerRow.createCell(cellIndex, CellType.STRING).setCellValue(Constants.COLNAME_GENERIC_ID);
    assignment.put(Constants.COLNAME_GENERIC_ID, cellIndex);
    ++cellIndex;

    headerRow
        .createCell(cellIndex, CellType.STRING)
        .setCellValue(Constants.Sheets.Validations.ValidationsNumeric.COLNAME_RANGE_FROM);
    assignment.put(Constants.Sheets.Validations.ValidationsNumeric.COLNAME_RANGE_FROM, cellIndex);
    ++cellIndex;

    headerRow
        .createCell(cellIndex, CellType.STRING)
        .setCellValue(Constants.Sheets.Validations.ValidationsNumeric.COLNAME_RANGE_TO);
    assignment.put(Constants.Sheets.Validations.ValidationsNumeric.COLNAME_RANGE_TO, cellIndex);
    ++cellIndex;

    headerRow
        .createCell(cellIndex, CellType.STRING)
        .setCellValue(Constants.Sheets.Validations.ValidationsNumeric.COLNAME_UNIT_OF_MEASURE);
    assignment.put(
        Constants.Sheets.Validations.ValidationsNumeric.COLNAME_UNIT_OF_MEASURE, cellIndex);

    return assignment;
  }

  public boolean isWithinRange() {
    return withinRange;
  }

  public void setWithinRange(boolean withinRange) {
    this.withinRange = withinRange;
  }

  public Double getRangeFrom() {
    return rangeFrom;
  }

  public void setRangeFrom(Double rangeFrom) {
    this.rangeFrom = rangeFrom;
  }

  public Double getRangeTo() {
    return rangeTo;
  }

  public void setRangeTo(Double rangeTo) {
    this.rangeTo = rangeTo;
  }

  public String getUnitOfMeasure() {
    return unitOfMeasure;
  }

  public void setUnitOfMeasure(String unitOfMeasure) {
    this.unitOfMeasure = unitOfMeasure;
  }

  @Override
  public String toString() {
    return "FloatValidation{"
        + "withinRange="
        + withinRange
        + ", rangeFrom="
        + rangeFrom
        + ", rangeTo="
        + rangeTo
        + ", unitOfMeasure='"
        + unitOfMeasure
        + '\''
        + '}';
  }
}
