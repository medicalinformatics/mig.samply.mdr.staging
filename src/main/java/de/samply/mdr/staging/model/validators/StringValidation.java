/*
 * Copyright (C) 2018 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.staging.model.validators;

import de.samply.mdr.staging.Constants;
import de.samply.mdr.staging.model.enums.ValidationType;
import de.samply.mdr.staging.model.source.excel.Validation;
import de.samply.mdr.staging.utils.ExcelUtil;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * String Validation.
 *
 * <p>Contains maxlength as well as regular expression.
 */
public class StringValidation extends Validation {

  private int maxLength;
  private String regex;

  public StringValidation() {
    setValidationType(ValidationType.VT_STRING);
  }

  /**
   * Read the String Validations from the given workbook.
   *
   * @param workbook the workbook to read from
   * @return a map containing the validationtype and id as key and the validation itself as value
   */
  public static Map<AbstractMap.SimpleEntry<ValidationType, Integer>, Validation> readFromWorkbook(
      Workbook workbook) {
    Map<AbstractMap.SimpleEntry<ValidationType, Integer>, Validation> validations = new HashMap<>();

    Sheet sheet = workbook.getSheet(Constants.Sheets.Validations.ValidationsString.SHEETNAME);

    if (sheet == null) {
      return validations;
    }

    Row headerRow = sheet.getRow(Constants.ROW_INDEX_HEADER);
    Map<String, Integer> columnAssignments = ExcelUtil.getColumnAssignmentsFromHeader(headerRow);

    Iterator<Row> rowIterator = sheet.rowIterator();

    if (rowIterator.hasNext()) {
      rowIterator.next(); // skip header row
    }

    while (rowIterator.hasNext()) {
      Row row = rowIterator.next();
      if (row.getPhysicalNumberOfCells() > 0) {
        try {
          StringValidation validation = new StringValidation();
          validation.setId(
              ExcelUtil.getCellValueAsInteger(
                  row.getCell(columnAssignments.get(Constants.Sheets.Validations.COLNAME_ID))));

          try {
            validation.setMaxLength(
                ExcelUtil.getCellValueAsInteger(
                    row.getCell(
                        columnAssignments.get(
                            Constants.Sheets.Validations.ValidationsString.COLNAME_MAXLENGTH))));
          } catch (Exception e) {
            validation.setMaxLength(-1);
          }

          validation.setRegex(
              ExcelUtil.getCellValueAsString(
                  row.getCell(
                      columnAssignments.get(
                          Constants.Sheets.Validations.ValidationsString.COLNAME_REGEX))));
          AbstractMap.SimpleEntry<ValidationType, Integer> mapKey =
              new AbstractMap.SimpleEntry<>(
                  ValidationType.VT_STRING,
                  ExcelUtil.getCellValueAsInteger(
                      row.getCell(columnAssignments.get(Constants.Sheets.Validations.COLNAME_ID))));
          validations.put(mapKey, validation);
        } catch (Exception e) {
          logger.debug(
              "Caught exception in line {} while trying to read string validation. "
                  + "Skipping this row.",
              row.getRowNum());
        }
      }
    }

    return validations;
  }

  /**
   * Create the header row and return the column assignment.
   *
   * @param sheet the xml sheet in which the rows shall be created
   * @return a map containing the assignment from header name to cell position
   */
  public static Map<String, Integer> createDefaultHeaderRow(Sheet sheet) {
    Map<String, Integer> assignment = new HashMap<>();
    int cellIndex = 0;

    Row headerRow = sheet.createRow(cellIndex);
    headerRow.createCell(cellIndex, CellType.STRING).setCellValue(Constants.COLNAME_GENERIC_ID);
    assignment.put(Constants.COLNAME_GENERIC_ID, cellIndex);
    ++cellIndex;

    headerRow
        .createCell(cellIndex, CellType.STRING)
        .setCellValue(Constants.Sheets.Validations.ValidationsString.COLNAME_MAXLENGTH);
    assignment.put(Constants.Sheets.Validations.ValidationsString.COLNAME_MAXLENGTH, cellIndex);
    ++cellIndex;

    headerRow
        .createCell(cellIndex, CellType.STRING)
        .setCellValue(Constants.Sheets.Validations.ValidationsString.COLNAME_REGEX);
    assignment.put(Constants.Sheets.Validations.ValidationsString.COLNAME_REGEX, cellIndex);

    return assignment;
  }

  public int getMaxLength() {
    return maxLength;
  }

  public void setMaxLength(int maxLength) {
    this.maxLength = maxLength;
  }

  public String getRegex() {
    return regex;
  }

  public void setRegex(String regex) {
    this.regex = regex;
  }

  @Override
  public String toString() {
    return "StringValidation{" + "maxLength=" + maxLength + ", regex='" + regex + '\'' + '}';
  }
}
