/*
 * Copyright (C) 2020 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.staging.model.source.excel;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import de.samply.mdr.staging.Constants;
import de.samply.mdr.staging.Constants.Sheets;
import de.samply.mdr.staging.utils.ExcelUtil;
import de.samply.mdr.xsd.staging.RelationType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConceptAssociation {

  /**
   * Represents one ConceptAssociation entry in an excel workbook.
   */
  private static final Logger logger = LoggerFactory
      .getLogger(de.samply.mdr.staging.model.source.excel.ConceptAssociation.class);

  private int dataElementId;
  private String system;
  private String version;
  private String term;
  private String text;
  private RelationType linktype;

  /**
   * Read all entries from the conceptAssociations sheet into a list.
   *
   * @param workbook the workbook, containing the slots sheet
   * @return a list containing all conceptAssociations
   */
  public static List<de.samply.mdr.staging.model.source.excel.ConceptAssociation>
      readListFromWorkbook(Workbook workbook) {
    List<de.samply.mdr.staging.model.source.excel.ConceptAssociation> conceptAssociations =
        new ArrayList<>();

    Sheet sheet = workbook.getSheet(Constants.Sheets.ConceptAssociation.SHEETNAME);

    if (sheet == null) {
      return conceptAssociations;
    }

    Row headerRow = sheet.getRow(Constants.ROW_INDEX_HEADER);
    Map<String, Integer> columnAssignments = ExcelUtil.getColumnAssignmentsFromHeader(headerRow);

    Iterator<Row> rowIterator = sheet.rowIterator();

    if (rowIterator.hasNext()) {
      rowIterator.next(); // skip header row
    }

    while (rowIterator.hasNext()) {
      Row row = rowIterator.next();
      if (row.getPhysicalNumberOfCells() > 0) {
        try {
          de.samply.mdr.staging.model.source.excel.ConceptAssociation conceptAssociation =
              new de.samply.mdr.staging.model.source.excel.ConceptAssociation();
          conceptAssociation.setDataElementId(
              ExcelUtil.getCellValueAsInteger(
                  row.getCell(
                      columnAssignments
                          .get(Constants.Sheets.ConceptAssociation.COLNAME_DATAELEMENT_ID))));
          conceptAssociation.setSystem(
              ExcelUtil.getCellValueAsString(
                  row.getCell(columnAssignments.get(Sheets.ConceptAssociation.COLNAME_SYSTEM))));
          conceptAssociation.setVersion(
              ExcelUtil.getCellValueAsString(
                  row.getCell(columnAssignments.get(Sheets.ConceptAssociation.COLNAME_VERSION))));
          conceptAssociation.setTerm(
              ExcelUtil.getCellValueAsString(
                  row.getCell(columnAssignments.get(Sheets.ConceptAssociation.COLNAME_TERM))));
          conceptAssociation.setText(
              ExcelUtil.getCellValueAsString(
                  row.getCell(columnAssignments.get(Sheets.ConceptAssociation.COLNAME_TEXT))));
          conceptAssociation.setLinktype(RelationType.fromValue(
              ExcelUtil.getCellValueAsString(
                  row.getCell(columnAssignments.get(Sheets.ConceptAssociation.COLNAME_LINKTYPE)))));
          conceptAssociations.add(conceptAssociation);
        } catch (NullPointerException npe) {
          logger.debug(
              "Caught null pointer exception while trying to read slot. Skipping this one...");
        }
      }
    }

    return conceptAssociations;
  }

  /**
   * Read all entries from the conceptAssociations sheet into a multimap.
   *
   * <p>This contains a certain redundancy (dataelement id is saved twice) but allows for more
   * convenient access.
   *
   * @param workbook the workbook, containing the conceptAssociations sheet
   * @return a multimap with the assignment elementid->conceptAssociations
   */
  public static Multimap<Integer, de.samply.mdr.staging.model.source.excel.ConceptAssociation>
      readMultiMapFromWorkbook(Workbook workbook) {
    Multimap<Integer, de.samply.mdr.staging.model.source.excel.ConceptAssociation>
        conceptAssociations = ArrayListMultimap.create();

    Sheet sheet = workbook.getSheet(Constants.Sheets.ConceptAssociation.SHEETNAME);

    if (sheet == null) {
      return conceptAssociations;
    }

    Row headerRow = sheet.getRow(Constants.ROW_INDEX_HEADER);
    Map<String, Integer> columnAssignments = ExcelUtil.getColumnAssignmentsFromHeader(headerRow);

    Iterator<Row> rowIterator = sheet.rowIterator();

    if (rowIterator.hasNext()) {
      rowIterator.next(); // skip header row
    }

    while (rowIterator.hasNext()) {
      Row row = rowIterator.next();
      if (row.getPhysicalNumberOfCells() > 0) {
        try {
          de.samply.mdr.staging.model.source.excel.ConceptAssociation conceptAssociation =
              new de.samply.mdr.staging.model.source.excel.ConceptAssociation();
          conceptAssociation.setDataElementId(
              ExcelUtil.getCellValueAsInteger(
                  row.getCell(
                      columnAssignments
                          .get(Constants.Sheets.ConceptAssociation.COLNAME_DATAELEMENT_ID))));
          conceptAssociation.setSystem(
              ExcelUtil.getCellValueAsString(
                  row.getCell(columnAssignments.get(Sheets.ConceptAssociation.COLNAME_SYSTEM))));
          conceptAssociation.setVersion(
              ExcelUtil.getCellValueAsString(
                  row.getCell(columnAssignments.get(Sheets.ConceptAssociation.COLNAME_VERSION))));
          conceptAssociation.setTerm(
              ExcelUtil.getCellValueAsString(
                  row.getCell(columnAssignments.get(Sheets.ConceptAssociation.COLNAME_TERM))));
          conceptAssociation.setText(
              ExcelUtil.getCellValueAsString(
                  row.getCell(columnAssignments.get(Sheets.ConceptAssociation.COLNAME_TEXT))));
          conceptAssociation.setLinktype(RelationType.fromValue(
              ExcelUtil.getCellValueAsString(
                  row.getCell(columnAssignments.get(Sheets.ConceptAssociation.COLNAME_LINKTYPE)))));
          conceptAssociations.put(conceptAssociation.getDataElementId(), conceptAssociation);
        } catch (Exception npe) {
          logger.debug(
              "Caught exception in line {} while trying to read slot. Skipping this row.",
              row.getRowNum());
        }
      }
    }

    return conceptAssociations;
  }

  /**
   * Create the header row and return the column assignment.
   *
   * @param sheet the xml sheet in which the rows shall be created
   * @return a map containing the assignment from header name to cell position
   */
  public static Map<String, Integer> createDefaultHeaderRow(Sheet sheet) {
    Map<String, Integer> assignment = new HashMap<>();
    int cellIndex = 0;

    Row headerRow = sheet.createRow(cellIndex);
    headerRow
        .createCell(cellIndex, CellType.STRING)
        .setCellValue(Constants.Sheets.ConceptAssociation.COLNAME_DATAELEMENT_ID);
    assignment.put(Constants.Sheets.ConceptAssociation.COLNAME_DATAELEMENT_ID, cellIndex);
    ++cellIndex;

    headerRow
        .createCell(cellIndex, CellType.STRING)
        .setCellValue(Sheets.ConceptAssociation.COLNAME_SYSTEM);
    assignment.put(Sheets.ConceptAssociation.COLNAME_SYSTEM, cellIndex);
    ++cellIndex;

    headerRow
        .createCell(cellIndex, CellType.STRING)
        .setCellValue(Sheets.ConceptAssociation.COLNAME_VERSION);
    assignment.put(Sheets.ConceptAssociation.COLNAME_VERSION, cellIndex);
    ++cellIndex;

    headerRow
        .createCell(cellIndex, CellType.STRING)
        .setCellValue(Sheets.ConceptAssociation.COLNAME_TERM);
    assignment.put(Sheets.ConceptAssociation.COLNAME_TERM, cellIndex);
    ++cellIndex;

    headerRow
        .createCell(cellIndex, CellType.STRING)
        .setCellValue(Sheets.ConceptAssociation.COLNAME_TEXT);
    assignment.put(Sheets.ConceptAssociation.COLNAME_TEXT, cellIndex);
    ++cellIndex;

    headerRow
        .createCell(cellIndex, CellType.STRING)
        .setCellValue(Sheets.ConceptAssociation.COLNAME_LINKTYPE);
    assignment.put(Sheets.ConceptAssociation.COLNAME_LINKTYPE, cellIndex);
    ++cellIndex;

    return assignment;
  }

  public int getDataElementId() {
    return dataElementId;
  }

  public void setDataElementId(int dataElementId) {
    this.dataElementId = dataElementId;
  }

  public String getSystem() {
    return system;
  }

  public void setSystem(String system) {
    this.system = system;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public String getTerm() {
    return term;
  }

  public void setTerm(String term) {
    this.term = term;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public RelationType getLinktype() {
    return linktype;
  }

  public void setLinktype(RelationType linktype) {
    this.linktype = linktype;
  }

  @Override
  public int hashCode() {
    return super.hashCode();
  }

  @Override
  public boolean equals(Object o) {
    return super.equals(o);
  }

  @Override
  public String toString() {
    return "ConceptAssociation{"
        + "dataElementId="
        + dataElementId
        + ", system='"
        + system
        + '\''
        + ", version='"
        + version
        + '\''
        + ", term='"
        + term
        + '\''
        + ", text='"
        + text
        + '\''
        + ", linktype='"
        + linktype
        + '\''
        + '}';
  }

}
