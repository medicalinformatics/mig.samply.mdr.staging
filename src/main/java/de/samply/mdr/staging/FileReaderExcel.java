/*
 * Copyright (C) 2018 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.staging;

import com.google.common.collect.Multimap;
import de.samply.mdr.staging.model.enums.ValidationType;
import de.samply.mdr.staging.model.source.excel.ConceptAssociation;
import de.samply.mdr.staging.model.source.excel.Dataelement;
import de.samply.mdr.staging.model.source.excel.Definition;
import de.samply.mdr.staging.model.source.excel.Info;
import de.samply.mdr.staging.model.source.excel.Slot;
import de.samply.mdr.staging.model.source.excel.Validation;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FileReaderExcel {

  private static final Logger logger = LoggerFactory.getLogger(FileReaderExcel.class);

  private Workbook workbook;
  private Info info;
  private Multimap<Integer, Slot> slots;
  private Multimap<Integer, ConceptAssociation> conceptAssociations;
  private Multimap<Integer, Definition> definitions;
  private Map<AbstractMap.SimpleEntry<ValidationType, Integer>, Validation> validations;
  private List<Dataelement> dataelements;

  /**
   * TODO: add javadoc.
   */
  public FileReaderExcel(String filename) {
    try {
      workbook = readImportFile(filename);
      readSheets();
    } catch (Exception e) {
      throw new IllegalArgumentException(e);
    }
  }

  /**
   * TODO: add javadoc.
   */
  public FileReaderExcel(File file) {
    try {
      workbook = readImportFile(file);
      readSheets();
    } catch (Exception e) {
      throw new IllegalArgumentException(e);
    }
  }

  private void readSheets() {
    info = Info.readFromWorkbook(workbook);
    slots = Slot.readMultiMapFromWorkbook(workbook);
    conceptAssociations = ConceptAssociation.readMultiMapFromWorkbook(workbook);
    definitions = Definition.readFromWorkbook(workbook);
    validations = Validation.readAllFromWorkbook(workbook, definitions);
    List<Dataelement> dataelementsFlat = Dataelement
        .readFromWorkbook(workbook, definitions, validations, slots, conceptAssociations);
    dataelements = buildHierarchy(dataelementsFlat);
  }

  private List<Dataelement> buildHierarchy(List<Dataelement> dataelementsFlat) {
    List<Dataelement> dataelements = new ArrayList<>();

    Iterator<Dataelement> dataelementIterator = dataelementsFlat.iterator();

    while (dataelementIterator.hasNext()) {
      Dataelement dataelement = dataelementIterator.next();
      if (dataelement.getParentId() == null) {
        dataelements.add(dataelement);
      } else {
        if (!addChildToElement(dataelement, dataelements)) {
          addChildToElement(dataelement, dataelementsFlat);
        }
      }
      dataelementIterator.remove();
    }

    return dataelements;
  }

  private boolean addChildToElement(Dataelement dataelement, List<Dataelement> dataelements) {
    int parentId = dataelement.getParentId();

    for (Dataelement de : dataelements) {
      if (de.getId() == parentId) {
        de.getChildren().add(dataelement);
        return true;
      } else if (de.getChildren().size() > 0) {
        if (addChildToElement(dataelement, de.getChildren())) {
          return true;
        }
      }
    }

    return false;
  }

  public Info getInfo() {
    return info;
  }

  public Multimap<Integer, Slot> getSlots() {
    return slots;
  }

  public Multimap<Integer, ConceptAssociation> getConceptAssociations() {
    return conceptAssociations;
  }

  public Multimap<Integer, Definition> getDefinitions() {
    return definitions;
  }

  public Map<AbstractMap.SimpleEntry<ValidationType, Integer>, Validation> getValidations() {
    return validations;
  }

  public List<Dataelement> getDataelements() {
    return dataelements;
  }

  private Workbook readImportFile(String filename) {
    return readImportFile(new File(filename));
  }

  private Workbook readImportFile(File file) {
    try {
      FileInputStream excelFile = new FileInputStream(file);
      return new XSSFWorkbook(excelFile);
    } catch (FileNotFoundException e) {
      logger.error("Could not find file: " + file.getName());
    } catch (IOException e) {
      logger.error("IOException caught", e);
    }
    return null;
  }
}
