/*
 * Copyright (C) 2018 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.staging.utils;

import static de.samply.mdr.xsd.staging.ElementType.DATAELEMENTGROUP;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.samply.mdr.dal.dto.Import;
import de.samply.mdr.dal.dto.Staging;
import de.samply.mdr.dal.jooq.enums.Elementtype;
import de.samply.mdr.lib.Constants;
import de.samply.mdr.staging.model.adapter.ValidationTypeAdapter;
import de.samply.mdr.staging.model.source.excel.Dataelement;
import de.samply.mdr.staging.model.source.excel.Validation;
import de.samply.mdr.xsd.staging.DefinitionType;
import de.samply.mdr.xsd.staging.StagedElementType;
import de.samply.string.util.StringUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class ImportToStagingConverter {

  private static Gson GSON;
  private static int currentId;

  static {
    GsonBuilder gsonBuilder = new GsonBuilder();
    gsonBuilder.registerTypeHierarchyAdapter(Validation.class, new ValidationTypeAdapter());
    gsonBuilder.disableHtmlEscaping();
    GSON = gsonBuilder.create();
  }

  /**
   * TODO: add javadoc.
   */
  public static Map<Integer, List<Staging>> excelDataelementToStaging(
      Import anImport, Dataelement dataelement) {
    Elementtype elementType;
    switch (dataelement.getElementType()) {
      case CATALOG:
        elementType = Elementtype.CATALOG;
        break;
      case DATAELEMENTGROUP:
        elementType = Elementtype.DATAELEMENTGROUP;
        break;
      case DATAELEMENT:
      default:
        elementType = Elementtype.DATAELEMENT;
        break;
    }

    String designation;
    try {
      designation = dataelement.getDefinitions().get(0).getDesignation();
    } catch (Exception e) {
      designation = "";
    }

    Staging stagedElement = new Staging();
    stagedElement.setImportId(anImport.getId());
    stagedElement.setDesignation(designation);
    stagedElement.setElementType(elementType);
    StagedElementType xmlStagedElement = ExcelToXmlConverter.convertDataelement(dataelement);
    stagedElement.setData(GSON.toJson(xmlStagedElement));
    return ImportToStagingConverter.xmlStagedelementToStaging(xmlStagedElement, anImport.getId());
  }

  public static Map<Integer, List<Staging>> xmlStagedelementToStaging(
      StagedElementType dataelement, int importId) {
    return xmlStagedelementToStaging(dataelement, importId, -1);
  }

  public static Map<Integer, List<Staging>> xmlStagedelementToStaging(
      StagedElementType dataelement, int importId, int parentId) {
    return xmlStagedelementToStaging(null, dataelement, importId, parentId);
  }

  public static Map<Integer, List<Staging>> xmlStagedelementToStaging(
      Map<Integer, List<Staging>> stagingMap,
      StagedElementType dataelement,
      int importId,
      int parentId) {
    currentId = 0;
    return convertElement(stagingMap, dataelement, importId, parentId, 0);
  }

  /**
   * TODO: add javadoc.
   */
  public static Map<Integer, List<Staging>> convertElement(
      Map<Integer, List<Staging>> stagingMap,
      StagedElementType dataelement,
      int importId,
      int parentId,
      int level) {
    if (stagingMap == null) {
      stagingMap = new TreeMap<>();
    }

    if (stagingMap.get(level) == null) {
      stagingMap.put(level, new ArrayList<Staging>());
    }

    String designation;
    try {
      designation = dataelement.getDefinitions().getDefinition().get(0).getDesignation();
    } catch (Exception e) {
      designation = "";
    }

    Staging staging;

    switch (dataelement.getElementType()) {
      case DATAELEMENTGROUP:
      case RECORD:
        staging = new Staging();
        staging.setId(currentId++);
        staging.setImportId(importId);
        staging.setDesignation(designation);
        if (dataelement.getElementType() == DATAELEMENTGROUP) {
          staging.setElementType(Elementtype.DATAELEMENTGROUP);
        } else {
          staging.setElementType(Elementtype.RECORD);
        }
        staging.setParentId(parentId);

        for (StagedElementType stagedElement : dataelement.getMembers().getStagedElement()) {
          convertElement(stagingMap, stagedElement, importId, staging.getId(), level + 1);
        }
        // The members have been converted to own elements. So delete them here.
        dataelement.setMembers(null);
        staging.setData(GSON.toJson(dataelement));
        stagingMap.get(level).add(staging);
        break;
      case DATAELEMENT:
      default:
        staging = new Staging();
        staging.setId(currentId++);
        staging.setImportId(importId);
        staging.setDesignation(designation);
        staging.setElementType(Elementtype.DATAELEMENT);
        staging.setData(GSON.toJson(dataelement));
        staging.setParentId(parentId);
        stagingMap.get(level).add(staging);
        break;
    }
    return stagingMap;
  }

  /**
   * If a language is submitted, try to return the definition in this language, return the first one
   * otherwise.
   *
   * @param stagedElement the staged element to get the definition from
   * @param language the desired language
   */
  public static DefinitionType getPrioritizedDefinition(
      StagedElementType stagedElement, Constants.Language language) {
    if (language != null && !StringUtil.isEmpty(language.getName())) {
      for (DefinitionType def : stagedElement.getDefinitions().getDefinition()) {
        if (language.getName().equalsIgnoreCase(def.getLanguage().value())) {
          return def;
        }
      }
    }
    return stagedElement.getDefinitions().getDefinition().get(0);
  }

  public static DefinitionType getPrioritizedDefinition(StagedElementType stagedElement) {
    return getPrioritizedDefinition(stagedElement, null);
  }
}
