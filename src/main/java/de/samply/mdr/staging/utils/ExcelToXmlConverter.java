/*
 * Copyright (C) 2018 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.staging.utils;

import de.samply.mdr.staging.model.source.excel.ConceptAssociation;
import de.samply.mdr.staging.model.source.excel.Dataelement;
import de.samply.mdr.staging.model.source.excel.Definition;
import de.samply.mdr.staging.model.source.excel.Slot;
import de.samply.mdr.staging.model.source.excel.Validation;
import de.samply.mdr.staging.model.validators.CalendarValidation;
import de.samply.mdr.staging.model.validators.CatalogValidation;
import de.samply.mdr.staging.model.validators.FloatValidation;
import de.samply.mdr.staging.model.validators.IntegerValidation;
import de.samply.mdr.staging.model.validators.PermittedValue;
import de.samply.mdr.staging.model.validators.PermittedValuesValidation;
import de.samply.mdr.staging.model.validators.StringValidation;
import de.samply.mdr.xsd.staging.ConceptAssociationType;
import de.samply.mdr.xsd.staging.ConceptAssociations;
import de.samply.mdr.xsd.staging.DateFormatString;
import de.samply.mdr.xsd.staging.DefinitionType;
import de.samply.mdr.xsd.staging.Definitions;
import de.samply.mdr.xsd.staging.LanguageType;
import de.samply.mdr.xsd.staging.Members;
import de.samply.mdr.xsd.staging.PermittedValueType;
import de.samply.mdr.xsd.staging.SlotType;
import de.samply.mdr.xsd.staging.Slots;
import de.samply.mdr.xsd.staging.StagedElementType;
import de.samply.mdr.xsd.staging.ValidationType;
import de.samply.mdr.xsd.staging.ValidationTypeEnum;
import java.util.ArrayList;
import java.util.List;

/**
 * Convert the entities read from excel to the format that is used for the REST API.
 *
 * <p>The differences are small-ish, mostly due to the need for "foreign keys" in the excel format,
 * since no inherent hierarchy is supplied.
 */
public class ExcelToXmlConverter {

  /**
   * Convert a dataelement read from an excel file to the format that is defined by the xsd files.
   *
   * @param excelElement a staged element in the excel-defined format
   * @return a dataelement in the xml-defined format
   */
  protected static StagedElementType convertDataelement(Dataelement excelElement) {
    StagedElementType xmlElement = new StagedElementType();
    xmlElement.setElementType(excelElement.getElementType());

    // For Groups or records, validation may be null
    if (excelElement.getValidation() != null) {
      xmlElement.setValidation(convertValidation(excelElement.getValidation()));
    }
    xmlElement.setDefinitions(convertDefinitions(excelElement.getDefinitions()));
    xmlElement.setSlots(convertSlots(excelElement.getSlots()));
    xmlElement
        .setConceptAssociations(convertConceptAssociations(excelElement.getConceptAssociations()));
    xmlElement.setMembers(convertChildren(excelElement.getChildren()));
    return xmlElement;
  }

  /**
   * Convert a list of dataelements to a Members object.
   *
   * @param children a list of dataelements in the excel format
   * @return a members object containing the list of staged elements
   */
  private static Members convertChildren(List<Dataelement> children) {
    Members members = new Members();

    for (Dataelement child : children) {
      members.getStagedElement().add(convertDataelement(child));
    }

    return members;
  }

  /**
   * Convert a list of slots from excel to xml format.
   *
   * @param excelSlots list of slots in excel format
   * @return a list with the slots in xml format
   */
  private static Slots convertSlots(List<Slot> excelSlots) {
    List<SlotType> xmlSlots = new ArrayList<>();

    for (Slot excelSlot : excelSlots) {
      SlotType xmlSlot = new SlotType();
      xmlSlot.setKey(excelSlot.getKey());
      xmlSlot.setValue(excelSlot.getValue());
      xmlSlots.add(xmlSlot);
    }

    Slots slots = new Slots();
    slots.getSlot().addAll(xmlSlots);
    return slots;
  }

  /**
   * Convert a list of conceptAssociations from excel to xml format.
   *
   * @param excelConceptAssociations list of conceptAssociations in excel format
   * @return a list with the conceptAssociations in xml format
   */
  private static ConceptAssociations convertConceptAssociations(
      List<ConceptAssociation> excelConceptAssociations) {
    List<ConceptAssociationType> xmlConceptAssociations = new ArrayList<>();

    for (ConceptAssociation excelConceptAssociation : excelConceptAssociations) {
      ConceptAssociationType xmlConceptAssociation = new ConceptAssociationType();
      xmlConceptAssociation.setSystem(excelConceptAssociation.getSystem());
      xmlConceptAssociation.setVersion(excelConceptAssociation.getVersion());
      xmlConceptAssociation.setTerm(excelConceptAssociation.getTerm());
      xmlConceptAssociation.setText(excelConceptAssociation.getText());
      xmlConceptAssociation.setLinktype(excelConceptAssociation.getLinktype());
      xmlConceptAssociations.add(xmlConceptAssociation);
    }

    ConceptAssociations conceptAssociations = new ConceptAssociations();
    conceptAssociations.getConceptAssociation().addAll(xmlConceptAssociations);
    return conceptAssociations;
  }

  /**
   * Convert a list of definitions from excel to xml format.
   *
   * @param excelDefinitions list of definitions in excel format
   * @return a list with the definitions in xml format
   */
  private static Definitions convertDefinitions(List<Definition> excelDefinitions) {
    List<DefinitionType> xmlDefinitions = new ArrayList<>();

    for (Definition excelDefinition : excelDefinitions) {
      DefinitionType xmlDefinition = new DefinitionType();
      xmlDefinition.setDefinition(excelDefinition.getDefinition());
      xmlDefinition.setDesignation(excelDefinition.getDesignation());
      xmlDefinition.setLanguage(LanguageType.fromValue(excelDefinition.getLanguage().getName()));
      xmlDefinitions.add(xmlDefinition);
    }

    Definitions definitions = new Definitions();
    definitions.getDefinition().addAll(xmlDefinitions);

    return definitions;
  }

  /**
   * Convert validation from excel format to xml format.
   *
   * @param excelValidation the validation in the excel format
   * @return the validation in xml format
   */
  private static ValidationType convertValidation(Validation excelValidation)
      throws IllegalArgumentException {
    ValidationType xmlValidation;

    switch (excelValidation.getValidationType()) {
      case VT_BOOLEAN:
        xmlValidation = new de.samply.mdr.xsd.staging.BooleanValidation();
        xmlValidation.setValidationType(ValidationTypeEnum.BOOLEAN_VALIDATION);
        break;
      case VT_CALENDAR:
      case VT_DATETIME:
      case VT_DATE:
      case VT_TIME:
        xmlValidation = new de.samply.mdr.xsd.staging.CalendarValidation();
        xmlValidation.setValidationType(ValidationTypeEnum.CALENDAR_VALIDATION);
        DateFormatString dfs =
            DateFormatString.fromValue(
                ((CalendarValidation) excelValidation).getDateTimeFormat().getFormatString());
        ((de.samply.mdr.xsd.staging.CalendarValidation) xmlValidation).setDateFormat(dfs);
        break;
      case VT_CATALOG:
        xmlValidation = new de.samply.mdr.xsd.staging.CatalogValidation();
        xmlValidation.setValidationType(ValidationTypeEnum.CATALOG_VALIDATION);
        ((de.samply.mdr.xsd.staging.CatalogValidation) xmlValidation)
            .setCatalogUrn(((CatalogValidation) excelValidation).getCatalogUrn());
        break;
      case VT_FLOAT:
        xmlValidation = new de.samply.mdr.xsd.staging.FloatValidation();
        xmlValidation.setValidationType(ValidationTypeEnum.FLOAT_VALIDATION);
        ((de.samply.mdr.xsd.staging.FloatValidation) xmlValidation)
            .setRangeFrom(((FloatValidation) excelValidation).getRangeFrom());
        ((de.samply.mdr.xsd.staging.FloatValidation) xmlValidation)
            .setRangeTo(((FloatValidation) excelValidation).getRangeTo());
        ((de.samply.mdr.xsd.staging.FloatValidation) xmlValidation)
            .setUnitOfMeasure(((FloatValidation) excelValidation).getUnitOfMeasure());
        ((de.samply.mdr.xsd.staging.FloatValidation) xmlValidation)
            .setWithinRange(((FloatValidation) excelValidation).isWithinRange());
        break;
      case VT_INTEGER:
        xmlValidation = new de.samply.mdr.xsd.staging.IntegerValidation();
        xmlValidation.setValidationType(ValidationTypeEnum.INTEGER_VALIDATION);
        ((de.samply.mdr.xsd.staging.IntegerValidation) xmlValidation)
            .setRangeFrom(((IntegerValidation) excelValidation).getRangeFrom());
        ((de.samply.mdr.xsd.staging.IntegerValidation) xmlValidation)
            .setRangeTo(((IntegerValidation) excelValidation).getRangeTo());
        ((de.samply.mdr.xsd.staging.IntegerValidation) xmlValidation)
            .setUnitOfMeasure(((IntegerValidation) excelValidation).getUnitOfMeasure());
        ((de.samply.mdr.xsd.staging.IntegerValidation) xmlValidation)
            .setWithinRange(((IntegerValidation) excelValidation).isWithinRange());
        break;
      case VT_ENUMERATED:
        xmlValidation = new de.samply.mdr.xsd.staging.PermittedValuesValidation();
        xmlValidation.setValidationType(ValidationTypeEnum.PERMITTED_VALUES_VALIDATION);
        ((de.samply.mdr.xsd.staging.PermittedValuesValidation) xmlValidation)
            .getPermittedValue()
            .addAll(
                convertPermittedValues(
                    ((PermittedValuesValidation) excelValidation).getPermittedValues()));
        break;
      case VT_STRING:
        xmlValidation = new de.samply.mdr.xsd.staging.StringValidation();
        xmlValidation.setValidationType(ValidationTypeEnum.STRING_VALIDATION);
        ((de.samply.mdr.xsd.staging.StringValidation) xmlValidation)
            .setMaxLength((long) ((StringValidation) excelValidation).getMaxLength());
        ((de.samply.mdr.xsd.staging.StringValidation) xmlValidation)
            .setRegex(((StringValidation) excelValidation).getRegex());
        break;
      case VT_TBD:
      default:
        xmlValidation = new de.samply.mdr.xsd.staging.TbdValidation();
        xmlValidation.setValidationType(ValidationTypeEnum.TBD_VALIDATION);
    }

    return xmlValidation;
  }

  /**
   * Convert a list of permitted values from excel format to xml format.
   *
   * @param excelPermittedValues list of permitted values in excel format
   * @return a list of permitted values in xml format
   */
  private static List<PermittedValueType> convertPermittedValues(
      List<PermittedValue> excelPermittedValues) {
    List<PermittedValueType> xmlPermittedValues = new ArrayList<>();

    for (PermittedValue excelPermittedValue : excelPermittedValues) {
      PermittedValueType xmlPermittedValue = new PermittedValueType();
      xmlPermittedValue.setValue(excelPermittedValue.getValue());
      xmlPermittedValue.setDefinitions(convertDefinitions(excelPermittedValue.getDefinitions()));
      xmlPermittedValues.add(xmlPermittedValue);
    }

    return xmlPermittedValues;
  }
}
