/*
 * Copyright (C) 2018 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.staging.utils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Utility class to mostly read from excel workbooks. */
public class ExcelUtil {

  private static final Logger logger = LoggerFactory.getLogger(ExcelUtil.class);
  private static DataFormatter dataFormatter = new DataFormatter();

  // Prevent instantiation
  private ExcelUtil() {}

  /**
   * Read one cell and return the value as string.
   *
   * @param cell the cell to read from
   * @return the cell value as string
   */
  public static String getCellValueAsString(Cell cell) {
    return dataFormatter.formatCellValue(cell);
  }

  /**
   * Read one cell and return the value as integer.
   *
   * @param cell the cell to read from
   * @return the cell value as integer, or null on error
   */
  public static Integer getCellValueAsInteger(Cell cell) {
    if (cell == null) {
      return null;
    }
    switch (cell.getCellTypeEnum()) {
      case STRING:
        String cellValueString = cell.getStringCellValue();
        if (cellValueString == null || !cellValueString.matches("-?\\d+")) {
          return null;
        }
        return Integer.valueOf(cellValueString);
      case NUMERIC:
        return (int) cell.getNumericCellValue();
      case BLANK:
        logger.trace(
            "Blank cell when trying to read integer. This should not be an issue. Returning null");
        return null;
      default:
        logger.warn("Unexpected cell type: " + cell.getCellTypeEnum());
        return null;
    }
  }

  /**
   * Read one cell and return the value as long.
   *
   * @param cell the cell to read from
   * @return the cell value as long, or null on error
   */
  public static Long getCellValueAsLong(Cell cell) {
    if (cell == null) {
      return null;
    }

    switch (cell.getCellTypeEnum()) {
      case STRING:
        String cellValueString = cell.getStringCellValue();
        if (cellValueString == null || !cellValueString.matches("-?\\d+")) {
          return null;
        }
        return Long.valueOf(cellValueString);
      case NUMERIC:
        return (long) cell.getNumericCellValue();
      case BLANK:
        logger.trace(
            "Blank cell when trying to read long. This should not be an issue. Returning null");
        return null;
      default:
        logger.warn("Unexpected cell type: " + cell.getCellTypeEnum());
        return null;
    }
  }

  /**
   * Read one cell and return the value as double.
   *
   * @param cell the cell to read from
   * @return the cell value as double, or null on error
   */
  public static Double getCellValueAsDouble(Cell cell) {
    if (cell == null) {
      return null;
    }

    switch (cell.getCellTypeEnum()) {
      case STRING:
        String cellValueString = cell.getStringCellValue();
        if (cellValueString == null || !cellValueString.matches("-?\\d+(\\.\\d+)?")) {
          return null;
        }
        return Double.valueOf(cellValueString);
      case NUMERIC:
        return cell.getNumericCellValue();
      case BLANK:
        logger.trace(
            "Blank cell when trying to read double. This should not be an issue. Returning null");
        return null;
      default:
        logger.warn("Unexpected cell type: " + cell.getCellTypeEnum());
        return null;
    }
  }

  /**
   * Get the column indices from the header row.
   *
   * @param headerRow the header row
   * @return a map, assigning the cell value to the cell index
   */
  public static Map<String, Integer> getColumnAssignmentsFromHeader(Row headerRow) {
    Map<String, Integer> assignments = new HashMap<>();

    Iterator<Cell> cellIterator = headerRow.cellIterator();

    while (cellIterator.hasNext()) {
      Cell cell = cellIterator.next();
      if (cell.getCellTypeEnum() == CellType.STRING) {
        assignments.put(cell.getStringCellValue(), cell.getColumnIndex());
      }
    }

    return assignments;
  }

  /** Print all rows on a sheet. */
  public static void printrows(Sheet sheet) {
    Iterator<Row> rowIterator = sheet.rowIterator();

    while (rowIterator.hasNext()) {
      Row row = rowIterator.next();
      Iterator<Cell> cellIterator = row.cellIterator();
      while (cellIterator.hasNext()) {
        Cell cell = cellIterator.next();
        switch (cell.getCellTypeEnum()) {
          case STRING:
            logger.info(cell.getStringCellValue());
            break;
          case NUMERIC:
            logger.info(Double.toString(cell.getNumericCellValue()));
            break;
          case BOOLEAN:
            logger.info(Boolean.toString(cell.getBooleanCellValue()));
            break;
          default:
            logger.warn("Unexpected cell type: " + cell.getCellTypeEnum());
            break;
        }
      }
    }
  }

  /**
   * Automatically set the size of each column in the workbook.
   *
   * <p>calls {@link Sheet#autoSizeColumn(int)} for each column.
   *
   * @param workBook the workbook to set the column size in
   */
  public static void autosizeAllColumns(Workbook workBook) {
    for (int i = 0; i < workBook.getNumberOfSheets(); i++) {
      Sheet sheet = workBook.getSheetAt(i);
      Row headerRow = sheet.getRow(0);
      if (headerRow != null) {
        for (int j = 0; j <= headerRow.getLastCellNum(); j++) {
          sheet.autoSizeColumn(j);
        }
      }
    }
  }
}
