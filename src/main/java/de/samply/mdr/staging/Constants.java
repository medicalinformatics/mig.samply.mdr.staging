/*
 * Copyright (C) 2018 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.staging;

/**
 * A collection of constants for the used strings in an importable excel workbook.
 *
 * <p>Contains names for the sheets and columns.
 */
public class Constants {

  public static final int ROW_INDEX_HEADER = 0;

  public static final String NAME_SHEET_INFO = "Info";
  public static final String NAME_SHEET_DATAELEMENTS = "dataelements";
  public static final String NAME_SHEET_SLOTS = "slots";
  public static final String NAME_SHEET_CONCEPTASSOCIATION = "conceptAssociations";
  public static final String NAME_SHEET_DEFINITIONS = "definitions";
  public static final String NAME_SHEET_VALIDATIONS_PERMITTEDVALUES = "validations_permittedValues";
  public static final String NAME_SHEET_VALIDATIONS_STRING = "validations_string";
  public static final String NAME_SHEET_VALIDATIONS_INTEGER = "validations_integer";
  public static final String NAME_SHEET_VALIDATIONS_FLOAT = "validations_float";
  public static final String NAME_SHEET_VALIDATIONS_CALENDAR = "validations_calendar";
  public static final String NAME_SHEET_VALIDATIONS_DATE = "validations_date";
  public static final String NAME_SHEET_VALIDATIONS_TIME = "validations_time";
  public static final String NAME_SHEET_VALIDATIONS_DATETIME = "validations_datetime";
  public static final String NAME_SHEET_VALIDATIONS_CATALOG = "validations_catalog";

  public static final String COLNAME_GENERIC_ID = "id";
  public static final String COLNAME_GENERIC_TEMP_ID = "temp_id";

  public static final String COLNAME_GENERIC_DEFINITION_ID = "definition_id";

  public class Sheets {

    public class Info {

      public static final String SHEETNAME = NAME_SHEET_INFO;
      public static final String COLNAME_NAMESPACE = "namespace_name";
      public static final String COLNAME_IMPORT_LABEL = "label";
      public static final String COLNAME_IMPORT_STRATEGY = "import_strategy";
    }

    public class Dataelements {

      public static final String SHEETNAME = NAME_SHEET_DATAELEMENTS;
      public static final String COLNAME_ID = COLNAME_GENERIC_ID;
      public static final String COLNAME_TEMP_ID = COLNAME_GENERIC_TEMP_ID;
      public static final String COLNAME_PARENT_ID = "parent_id";
      public static final String COLNAME_ELEMENT_TYPE = "element_type";
      public static final String COLNAME_DEFINITION_ID = COLNAME_GENERIC_DEFINITION_ID;
      public static final String COLNAME_VALIDATION_TYPE = "validation_type";
      public static final String COLNAME_VALIDATION_ID = "validation_id";
    }

    public class Slots {

      public static final String SHEETNAME = NAME_SHEET_SLOTS;
      public static final String COLNAME_DATAELEMENT_ID = "dataelement_id";
      public static final String COLNAME_KEY = "key";
      public static final String COLNAME_VALUE = "value";
    }

    public class ConceptAssociation {

      public static final String SHEETNAME = NAME_SHEET_CONCEPTASSOCIATION;
      public static final String COLNAME_DATAELEMENT_ID = "dataelement_id";
      public static final String COLNAME_SYSTEM = "system";
      public static final String COLNAME_VERSION = "version";
      public static final String COLNAME_TERM = "term";
      public static final String COLNAME_TEXT = "text";
      public static final String COLNAME_LINKTYPE = "linktype";
    }

    public class Definitions {

      public static final String SHEETNAME = NAME_SHEET_DEFINITIONS;
      public static final String COLNAME_ID = COLNAME_GENERIC_ID;
      public static final String COLNAME_LANGUAGE = "language";
      public static final String COLNAME_DESIGNATION = "designation";
      public static final String COLNAME_DEFINITION = "definition";
    }

    public class Validations {

      public static final String COLNAME_ID = COLNAME_GENERIC_ID;

      public class PermittedValues {

        public static final String SHEETNAME = NAME_SHEET_VALIDATIONS_PERMITTEDVALUES;
        public static final String COLNAME_VALUE = "value";
        public static final String COLNAME_DEFINITION_ID = COLNAME_GENERIC_DEFINITION_ID;
      }

      public class ValidationsString {

        public static final String SHEETNAME = NAME_SHEET_VALIDATIONS_STRING;
        public static final String COLNAME_MAXLENGTH = "max_length";
        public static final String COLNAME_REGEX = "regex";
      }

      public class ValidationsNumeric {

        public static final String COLNAME_RANGE_FROM = "range_from";
        public static final String COLNAME_RANGE_TO = "range_to";
        public static final String COLNAME_UNIT_OF_MEASURE = "unit_of_measure";

        public class ValidationsInteger {

          public static final String SHEETNAME = NAME_SHEET_VALIDATIONS_INTEGER;
        }

        public class ValidationsFloat {

          public static final String SHEETNAME = NAME_SHEET_VALIDATIONS_FLOAT;
        }
      }

      public class ValidationsCalendar {

        public static final String SHEETNAME = NAME_SHEET_VALIDATIONS_CALENDAR;
        public static final String COLNAME_FORMAT = "format";

        public class ValidationsDate {

          public static final String SHEETNAME = NAME_SHEET_VALIDATIONS_DATE;
        }

        public class ValidationsTime {

          public static final String SHEETNAME = NAME_SHEET_VALIDATIONS_TIME;
        }

        public class ValidationsDateTime {

          public static final String SHEETNAME = NAME_SHEET_VALIDATIONS_DATETIME;
        }
      }

      public class ValidationsCatalog {

        public static final String SHEETNAME = NAME_SHEET_VALIDATIONS_CATALOG;
        public static final String COLNAME_CATALOG_URN = "catalog_urn";
      }
    }
  }
}
