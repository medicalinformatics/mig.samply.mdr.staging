/*
 * Copyright (C) 2019 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.staging;

import static junit.framework.TestCase.assertTrue;

import de.samply.mdr.staging.model.source.excel.Dataelement;
import de.samply.mdr.staging.model.source.excel.Info;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Test if a non-empty workbook can be read and results in dataelements
 */
public class FileReaderExcelTest {

  public static final String INPUT_FILENAME = "src/test/resources/generated.xlsx";
  FileReaderExcel fileReaderExcel;

  @Before
  public void setUp() {
    fileReaderExcel = new FileReaderExcel(INPUT_FILENAME);
  }

  @After
  public void tearDown() {
  }

  @Test
  public void testCreateDataelements() {
    List<Dataelement> dataelements = fileReaderExcel.getDataelements();
    assertTrue(dataelements != null && dataelements.size() > 0);
    System.out.println(dataelements.size() + " rootlevel-elements found in the excel sheet");
  }

  @Test
  public void testReadInfoSheet() {
    Info info = fileReaderExcel.getInfo();
    assertTrue(info.getNamespaceName() != null && info.getNamespaceName().length() > 0);
    System.out.println(info.getNamespaceName());
  }
}
