/*
 * Copyright (C) 2019 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.staging;

import de.samply.config.util.JAXBUtil;
import de.samply.mdr.xsd.staging.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

public class JaxbClassesTest {
  private static final String CHAR_LIST =
      "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

  @Before
  public void setUp() throws Exception {}

  @After
  public void tearDown() throws Exception {}

  public void testAll(String[] args)
      throws JAXBException, FileNotFoundException, ParserConfigurationException, SAXException {
    testCreateElement();
    testCreateGroup();
    testReadElement();
    testReadGroup();
    //        Locale.getISOLanguages();
  }

  @Test
  public void testCreateElement() throws JAXBException {
    StagedElementType stagedElement = createStagedElement();
    System.out.println("========================");
    System.out.println("======== WRITING =======");
    System.out.println("======== ELEMENT =======");
    System.out.println("========================\n");
    System.out.println("======== XML =======\n");
    System.out.println(stagedElementToXml(stagedElement));
  }

  @Test
  public void testCreateGroup() throws JAXBException {
    StagedElementType stagedGroup = createRandomStagedGroup(true);
    System.out.println("========================");
    System.out.println("======== WRITING =======");
    System.out.println("========= GROUP ========");
    System.out.println("========================\n");
    System.out.println("======== XML =======\n");
    System.out.println(stagedElementToXml(stagedGroup));
    writeToDisk(stagedGroup);
  }

  @Test
  public void testCreateRecord() throws JAXBException {
    StagedElementType stagedRecord = createRandomRecord();
    System.out.println("========================");
    System.out.println("======== WRITING =======");
    System.out.println("========= RECORD ========");
    System.out.println("========================\n");
    System.out.println("======== XML =======\n");
    System.out.println(stagedElementToXml(stagedRecord));
    writeToDisk(stagedRecord);
  }

  @Test
  public void testReadElement()
      throws JAXBException, FileNotFoundException, SAXException, ParserConfigurationException {
    System.out.println("========================");
    System.out.println("======== READING =======");
    System.out.println("======== ELEMENT =======");
    System.out.println("========================\n");
    System.out.println("======== XML =======\n");
    StagedElementType fromXml = fromXml("src/test/resources/test-stagedelement.xml");
    System.out.println("Validation Type: " + fromXml.getValidation().getValidationType());
    for (DefinitionType definition : fromXml.getDefinitions().getDefinition()) {
      if (definition.getLanguage().value().equalsIgnoreCase("fr")) {
        System.out.println("French designation: " + definition.getDesignation());
      }
    }
  }

  @Test
  public void testReadGroup()
      throws JAXBException, FileNotFoundException, SAXException, ParserConfigurationException {
    System.out.println("========================");
    System.out.println("======== READING =======");
    System.out.println("========= GROUP ========");
    System.out.println("========================\n");
    System.out.println("======== XML =======\n");
    StagedElementType grpFromXml = fromXml("src/test/resources/test-stagedelementgroup.xml");
    for (DefinitionType definition : grpFromXml.getDefinitions().getDefinition()) {
      if (definition.getLanguage().value().equalsIgnoreCase("de")) {
        System.out.println("German designation: " + definition.getDesignation());
      }
    }
  }

  public static StagedElementType createRandomRecord() {
    StagedElementType stagedRecord = new StagedElementType();
    stagedRecord.setElementType(ElementType.RECORD);
    stagedRecord.setDefinitions(createRandomDefinitions("de", "en"));

    Members members = new Members();

    int count = ThreadLocalRandom.current().nextInt(2, 5);

    for (int i = 0; i < count; ++i) {
      members.getStagedElement().add(createStagedElement());
    }

    stagedRecord.setMembers(members);

    return stagedRecord;
  }

  public static StagedElementType createRandomStagedGroup(boolean forceSubGroup) {
    StagedElementType stagedGroup = new StagedElementType();
    stagedGroup.setElementType(ElementType.DATAELEMENTGROUP);
    stagedGroup.setDefinitions(createRandomDefinitions("de", "en"));

    Members members = new Members();

    int count = ThreadLocalRandom.current().nextInt(1, 20);

    for (int i = 0; i < count; ++i) {
      members.getStagedElement().add(createStagedElement());
    }

    if (forceSubGroup || ThreadLocalRandom.current().nextInt(0, 100) > 90) {
      members.getStagedElement().add(createRandomStagedGroup(false));
    }

    stagedGroup.setMembers(members);

    return stagedGroup;
  }

  public static StagedElementType createStagedElement() {
    StagedElementType stagedElement = new StagedElementType();
    stagedElement.setElementType(ElementType.DATAELEMENT);
    stagedElement.setValidation(createRandomValidation());
    stagedElement.setDefinitions(createRandomDefinitions("de", "en", "fr"));
    stagedElement.setSlots(createRandomSlots());
    return stagedElement;
  }

  private static Slots createRandomSlots() {
    Slots slots = new Slots();

    int count = ThreadLocalRandom.current().nextInt(0, 5);

    for (int i = 0; i < count; ++i) {
      SlotType slot = new SlotType();
      slot.setKey(generateRandomString(ThreadLocalRandom.current().nextInt(5, 20)));
      slot.setValue(generateRandomString(ThreadLocalRandom.current().nextInt(5, 20)));
      slots.getSlot().add(slot);
    }
    return slots;
  }

  private static PermittedValuesValidation createPermittedValuesValidation() {
    PermittedValuesValidation validation = new PermittedValuesValidation();
    validation.setValidationType(ValidationTypeEnum.PERMITTED_VALUES_VALIDATION);

    int count = ThreadLocalRandom.current().nextInt(5, 20);

    for (int i = 0; i < count; ++i) {
      PermittedValueType pv = new PermittedValueType();
      pv.setValue(generateRandomString(ThreadLocalRandom.current().nextInt(5, 20)));
      pv.setDefinitions(createRandomDefinitions("en", "de"));
      validation.getPermittedValue().add(pv);
    }

    return validation;
  }

  private static ValidationType createRandomValidation() {
    int number = ThreadLocalRandom.current().nextInt(0, 7);
    ValidationType validation;
    if (number == 0) {
      validation = new BooleanValidation();
      validation.setValidationType(ValidationTypeEnum.BOOLEAN_VALIDATION);
    } else if (number == 1) {
      validation = new CalendarValidation();
      validation.setValidationType(ValidationTypeEnum.CALENDAR_VALIDATION);
      ((CalendarValidation) validation)
          .setDateFormat(
              Arrays.asList(DateFormatString.values())
                  .get(
                      ThreadLocalRandom.current()
                          .nextInt(0, DateFormatString.values().length + 1)));
    } else if (number == 2) {
      validation = new CatalogValidation();
      validation.setValidationType(ValidationTypeEnum.CATALOG_VALIDATION);
      ((CatalogValidation) validation)
          .setCatalogUrn(generateRandomString(ThreadLocalRandom.current().nextInt(10, 20)));
    } else if (number == 3) {
      validation = new FloatValidation();
      validation.setValidationType(ValidationTypeEnum.FLOAT_VALIDATION);
      ((FloatValidation) validation).setWithinRange(true);
      ((FloatValidation) validation).setRangeTo(ThreadLocalRandom.current().nextDouble(1000.0));
      ((FloatValidation) validation)
          .setRangeFrom(
              ThreadLocalRandom.current().nextDouble(((FloatValidation) validation).getRangeTo()));
      ((FloatValidation) validation).setUnitOfMeasure("cm");
    } else if (number == 4) {
      validation = new IntegerValidation();
      validation.setValidationType(ValidationTypeEnum.INTEGER_VALIDATION);
      ((IntegerValidation) validation).setWithinRange(true);
      ((IntegerValidation) validation)
          .setRangeTo(1L + ThreadLocalRandom.current().nextLong(1000000));
      ((IntegerValidation) validation)
          .setRangeFrom(
              ThreadLocalRandom.current().nextLong(((IntegerValidation) validation).getRangeTo()));
      ((IntegerValidation) validation).setUnitOfMeasure("l");
    } else if (number == 5) {
      validation = new StringValidation();
      validation.setValidationType(ValidationTypeEnum.STRING_VALIDATION);
      ((StringValidation) validation).setMaxLength(ThreadLocalRandom.current().nextLong(50));
      ((StringValidation) validation)
          .setRegex(generateRandomString(ThreadLocalRandom.current().nextInt(20)));
    } else if (number == 6) {
      validation = createPermittedValuesValidation();
    } else {
      validation = null;
    }
    return validation;
  }

  public static String stagedElementToXml(StagedElementType stagedElement) throws JAXBException {
    JAXBContext jaxbContext = JAXBContext.newInstance(ObjectFactory.class);
    Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
    jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

    ObjectFactory objectFactory = new ObjectFactory();

    StringWriter sw = new StringWriter();
    jaxbMarshaller.marshal(objectFactory.createStagedElement(stagedElement), sw);
    return sw.toString();
  }

  public static StagedElementType fromXml(String filename)
      throws JAXBException, FileNotFoundException, SAXException, ParserConfigurationException {
    return JAXBUtil.unmarshall(
        new File(filename), JAXBContext.newInstance(ObjectFactory.class), StagedElementType.class);
  }

  public static void writeToDisk(StagedElementType stagedElement) throws JAXBException {
    File outfile = new File("src/test/resources/output.xml");
    JAXBContext jaxbContext = JAXBContext.newInstance(ObjectFactory.class);
    Marshaller marshaller = jaxbContext.createMarshaller();
    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

    ObjectFactory objectFactory = new ObjectFactory();

    marshaller.marshal(objectFactory.createStagedElement(stagedElement), outfile);
  }

  private static Definitions createRandomDefinitions(String... languages) {
    Definitions definitions = new Definitions();

    for (String language : languages) {
      DefinitionType def = new DefinitionType();
      def.setLanguage(LanguageType.fromValue(language));
      def.setDefinition(generateRandomString(50));
      def.setDesignation(generateRandomString(10));
      definitions.getDefinition().add(def);
    }
    return definitions;
  }

  private static String generateRandomString(int length) {
    StringBuilder randStr = new StringBuilder();
    for (int i = 0; i < length; i++) {
      int number = ThreadLocalRandom.current().nextInt(0, CHAR_LIST.length());
      char ch = CHAR_LIST.charAt(number);
      randStr.append(ch);
    }
    return randStr.toString();
  }
}
