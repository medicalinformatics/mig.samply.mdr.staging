/*
 * Copyright (C) 2019 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.staging;

import static junit.framework.TestCase.assertSame;

import de.samply.mdr.staging.model.enums.DateTimeFormat;
import de.samply.mdr.xsd.staging.DateFormatString;
import org.junit.Test;

public class DateTimeFormatTest {

  @Test
  public void testDateFormatFromString() {
    assertSame(
        DateTimeFormat.fromString(DateFormatString.YYYY_MM_DD.value()),
        DateTimeFormat.DATE_ISO8601_DAYS);
    assertSame(
        DateTimeFormat.fromString(DateFormatString.YYYY_MM.value()),
        DateTimeFormat.DATE_ISO8601_NO_DAYS);
    assertSame(
        DateTimeFormat.fromString(DateFormatString.DD_MM_YYYY.value()),
        DateTimeFormat.DATE_DIN5008_DAYS);
    assertSame(
        DateTimeFormat.fromString(DateFormatString.MM_YYYY.value()),
        DateTimeFormat.DATE_DIN5008_NO_DAYS);

    assertSame(
        DateTimeFormat.fromString(DateFormatString.HH_MM_SS.value()),
        DateTimeFormat.TIME_24H_SECONDS);
    assertSame(
        DateTimeFormat.fromString(DateFormatString.HH_MM.value()),
        DateTimeFormat.TIME_24H_NO_SECONDS);
    assertSame(
        DateTimeFormat.fromString(DateFormatString.HH_MM_SS_A.value()),
        DateTimeFormat.TIME_12H_SECONDS);
    assertSame(
        DateTimeFormat.fromString(DateFormatString.HH_MM_A.value()),
        DateTimeFormat.TIME_12H_NO_SECONDS);

    assertSame(
        DateTimeFormat.fromString(DateFormatString.YYYY_MM_DD_HH_MM_SS.value()),
        DateTimeFormat.DATETIME_ISO8601_DAYS_24H_SECONDS);
    assertSame(
        DateTimeFormat.fromString(DateFormatString.YYYY_MM_DD_HH_MM.value()),
        DateTimeFormat.DATETIME_ISO8601_DAYS_24H_NO_SECONDS);
    assertSame(
        DateTimeFormat.fromString(DateFormatString.YYYY_MM_DD_HH_MM_SS_A.value()),
        DateTimeFormat.DATETIME_ISO8601_DAYS_12H_SECONDS);
    assertSame(
        DateTimeFormat.fromString(DateFormatString.YYYY_MM_DD_HH_MM_A.value()),
        DateTimeFormat.DATETIME_ISO8601_DAYS_12H_NO_SECONDS);

    assertSame(
        DateTimeFormat.fromString(DateFormatString.DD_MM_YYYY_HH_MM_SS.value()),
        DateTimeFormat.DATETIME_DIN5008_DAYS_24H_SECONDS);
    assertSame(
        DateTimeFormat.fromString(DateFormatString.DD_MM_YYYY_HH_MM.value()),
        DateTimeFormat.DATETIME_DIN5008_DAYS_24H_NO_SECONDS);
    assertSame(
        DateTimeFormat.fromString(DateFormatString.DD_MM_YYYY_HH_MM_SS_A.value()),
        DateTimeFormat.DATETIME_DIN5008_DAYS_12H_SECONDS);
    assertSame(
        DateTimeFormat.fromString(DateFormatString.DD_MM_YYYY_HH_MM_A.value()),
        DateTimeFormat.DATETIME_DIN5008_DAYS_12H_NO_SECONDS);
  }
}
