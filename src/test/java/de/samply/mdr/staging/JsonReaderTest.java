/*
 * Copyright (C) 2019 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.staging;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import de.samply.mdr.staging.model.adapter.ValidationTypeAdapter;
import de.samply.mdr.xsd.staging.StagedElementType;
import de.samply.mdr.xsd.staging.ValidationType;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class JsonReaderTest {

  public static void main(String[] args) throws FileNotFoundException {
    GsonBuilder gsonBuilder = new GsonBuilder();
    gsonBuilder.registerTypeHierarchyAdapter(ValidationType.class, new ValidationTypeAdapter());
    gsonBuilder.disableHtmlEscaping();
    gsonBuilder.setPrettyPrinting();
    Gson gson = gsonBuilder.create();

    JsonReader reader = new JsonReader(new FileReader("src/test/resources/test-json-part.json"));

    StagedElementType stagedElement = gson.fromJson(reader, StagedElementType.class);

    String s = gson.toJson(stagedElement);
    System.out.println(s);
  }
}
