/*
 * Copyright (C) 2019 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.staging;

import de.samply.mdr.lib.Constants;
import de.samply.mdr.staging.model.enums.DateTimeFormat;
import de.samply.mdr.staging.model.enums.ValidationType;
import de.samply.mdr.staging.model.source.excel.Dataelement;
import de.samply.mdr.staging.model.source.excel.Definition;
import de.samply.mdr.staging.model.source.excel.Slot;
import de.samply.mdr.staging.model.source.excel.Validation;
import de.samply.mdr.staging.model.validators.*;
import de.samply.mdr.xsd.staging.ElementType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class FileWriterExcelTest {
  private static final Logger logger = LoggerFactory.getLogger(FileWriterExcelTest.class);
  public static final String OUTPUT_FILENAME = "src/test/resources/generated.xlsx";
  public static final int AMOUNT_OF_ROOTLVL_ENTRIES = 5;
  public static final int AMOUNT_OF_ROOTLVL_GROUPS = 1;
  FileWriterExcel fileWriterExcel;
  private static final String CHAR_LIST =
      "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
  private static final int RANDOM_STRING_LENGTH = 10;
  private static int idCounter = 0;
  private static int pvValidationId;
  private static int stringValidationId;
  private static int floatValidationId;
  private static int integerValidationId;
  private static int calendarValidationId;

  @Before
  public void setUp() {
    fileWriterExcel = new FileWriterExcel();
    pvValidationId = 1;
    stringValidationId = 1;
    floatValidationId = 1;
    integerValidationId = 1;
    calendarValidationId = 1;
  }

  @After
  public void tearDown() {}

  @Test
  public void testCreateWorkbook() throws IOException {
    List<Dataelement> dataelements = createDataelements();
    fileWriterExcel.createWorkbook(dataelements);
    fileWriterExcel.writeToFile(OUTPUT_FILENAME);
  }

  private List<Dataelement> createDataelements() {
    List<Dataelement> dataelements = new ArrayList<>();

    for (int j = 0; j < AMOUNT_OF_ROOTLVL_GROUPS; ++j) {
      dataelements.add(createRandomDataelementgroup(null));
    }
    for (int i = 0; i < AMOUNT_OF_ROOTLVL_ENTRIES; ++i) {
      dataelements.add(createRandomDataelement(null));
    }

    return dataelements;
  }

  private Dataelement createRandomDataelementgroup(Integer parentId) {
    Dataelement de = new Dataelement(ElementType.DATAELEMENTGROUP);
    de.setId(idCounter++);
    de.setDefinitions(createRandomDefinitions());
    de.setParentId(parentId);
    int subGroups = ThreadLocalRandom.current().nextInt(0, 2);
    int members = ThreadLocalRandom.current().nextInt(1, 5);

    for (int i = 0; i < members; ++i) {
      de.getChildren().add(createRandomDataelement(de.getId()));
    }

    for (int i = 0; i < subGroups; ++i) {
      de.getChildren().add(createRandomDataelementgroup(de.getId()));
    }

    return de;
  }

  private Dataelement createRandomDataelement(Integer parentId) {
    int randomNum = ThreadLocalRandom.current().nextInt(0, 6);
    if (randomNum == 0) {
      return createRandomDataelement(ValidationType.VT_BOOLEAN, parentId);
    } else if (randomNum == 1) {
      return createRandomDataelement(ValidationType.VT_CALENDAR, parentId);
    } else if (randomNum == 2) {
      return createRandomDataelement(ValidationType.VT_FLOAT, parentId);
    } else if (randomNum == 3) {
      return createRandomDataelement(ValidationType.VT_INTEGER, parentId);
    } else if (randomNum == 4) {
      return createRandomDataelement(ValidationType.VT_ENUMERATED, parentId);
    } else {
      return createRandomDataelement(ValidationType.VT_STRING, parentId);
    }
  }

  private Dataelement createRandomDataelement(ValidationType type, Integer parentId) {
    Dataelement de = new Dataelement(ElementType.DATAELEMENT);
    de.setDefinitions(createRandomDefinitions());
    de.setSlots(createRandomSlots());
    de.setId(idCounter++);
    de.setParentId(parentId);

    Validation validation;

    switch (type) {
      case VT_BOOLEAN:
        validation = new BooleanValidation();
        break;
      case VT_ENUMERATED:
        validation = new PermittedValuesValidation();
        validation.setId(pvValidationId++);
        ((PermittedValuesValidation) validation).setPermittedValues(createPermittedValues());
        break;
      case VT_INTEGER:
        validation = new IntegerValidation();
        validation.setValidationType(ValidationType.VT_INTEGER);
        validation.setId(integerValidationId++);
        ((IntegerValidation) validation)
            .setRangeFrom(ThreadLocalRandom.current().nextLong(0, 1000));
        ((IntegerValidation) validation)
            .setRangeTo(ThreadLocalRandom.current().nextLong(1001, 10000));
        ((IntegerValidation) validation).setUnitOfMeasure("cm");
        ((IntegerValidation) validation).setWithinRange(true);
        break;
      case VT_FLOAT:
        validation = new FloatValidation();
        validation.setValidationType(ValidationType.VT_FLOAT);
        validation.setId(floatValidationId++);
        ((FloatValidation) validation)
            .setRangeFrom(ThreadLocalRandom.current().nextDouble(0.0, 100.0));
        ((FloatValidation) validation)
            .setRangeTo(ThreadLocalRandom.current().nextDouble(100.1, 1000.0));
        ((FloatValidation) validation).setUnitOfMeasure("km");
        ((FloatValidation) validation).setWithinRange(true);
        break;
      case VT_CALENDAR:
        validation = new CalendarValidation();
        validation.setValidationType(ValidationType.VT_CALENDAR);
        validation.setId(calendarValidationId++);
        ((CalendarValidation) validation)
            .setDateTimeFormat(DateTimeFormat.DATETIME_DIN5008_DAYS_24H_SECONDS);
        break;
      case VT_STRING:
      default:
        validation = new StringValidation();
        validation.setValidationType(ValidationType.VT_STRING);
        validation.setId(stringValidationId++);
        ((StringValidation) validation).setMaxLength(ThreadLocalRandom.current().nextInt(10, 30));
        ((StringValidation) validation).setRegex(generateRandomString());
        break;
    }

    de.setValidation(validation);
    return de;
  }

  private List<PermittedValue> createPermittedValues() {
    List<PermittedValue> permittedValues = new ArrayList<>();
    for (int i = 0; i < ThreadLocalRandom.current().nextInt(10, 20); ++i) {
      permittedValues.add(createPermittedValue());
    }
    return permittedValues;
  }

  private PermittedValue createPermittedValue() {
    PermittedValue pv = new PermittedValue();

    pv.setValue(generateRandomString());
    pv.setDefinitions(createRandomDefinitions());

    return pv;
  }

  private List<Slot> createRandomSlots() {
    List<Slot> slots = new ArrayList<>();

    for (int i = 0; i < ThreadLocalRandom.current().nextInt(1, 5); ++i) {
      Slot slot = new Slot();
      slot.setValue(generateRandomString());
      slot.setKey(generateRandomString());
      slots.add(slot);
    }

    return slots;
  }

  private List<Definition> createRandomDefinitions() {
    List<Definition> definitions = new ArrayList<>();

    Definition deDefinition = new Definition();
    Definition enDefinition = new Definition();
    Definition frDefinition = new Definition();
    Definition esDefinition = new Definition();

    deDefinition.setLanguage(Constants.Language.DE);
    enDefinition.setLanguage(Constants.Language.EN);
    frDefinition.setLanguage(Constants.Language.FR);
    esDefinition.setLanguage(Constants.Language.ES);

    deDefinition.setDesignation(generateRandomString());
    deDefinition.setDefinition(generateRandomString());

    enDefinition.setDesignation(generateRandomString());
    enDefinition.setDefinition(generateRandomString());

    frDefinition.setDesignation(generateRandomString());
    frDefinition.setDefinition(generateRandomString());

    esDefinition.setDesignation(generateRandomString());
    esDefinition.setDefinition(generateRandomString());

    definitions.add(deDefinition);
    definitions.add(enDefinition);
    definitions.add(frDefinition);
    definitions.add(esDefinition);
    return definitions;
  }

  private String generateRandomString() {
    StringBuilder randStr = new StringBuilder();
    for (int i = 0; i < RANDOM_STRING_LENGTH; i++) {
      int number = ThreadLocalRandom.current().nextInt(0, CHAR_LIST.length());
      char ch = CHAR_LIST.charAt(number);
      randStr.append(ch);
    }
    return randStr.toString();
  }
}
